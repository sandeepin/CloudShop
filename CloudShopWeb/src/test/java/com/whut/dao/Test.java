package com.whut.dao;

import com.whut.dao.impl.HBaseRestDaoImpl;
import com.whut.domain.*;
import com.whut.util.Base64Util;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by YY on 2017-12-29.
 */
public class Test {
    //使用例子
    public  static void main(String[] args) throws JSONException {

        HBaseRestDaoImpl hBaseRestDao=new HBaseRestDaoImpl();
//        //查看所有的表名
//        System.out.println(hBaseRestDao.getHTableArray());
//
        //创建一张表
//        HbaseColumn hbaseColumn1 = new HbaseColumn("d");
//        HbaseTable hbaseTable =new HbaseTable(hbaseColumn1);
//        JSONObject jsonObject = hbaseTable.getHbaseTableInJson();
//        System.out.print(jsonObject);
//        System.out.println(hBaseRestDao.createHTableByJson("wuzhong",jsonObject));

//        //修改表结构
//        HbaseColumn hbaseColumn3 = new HbaseColumn("family3");
//        HbaseTable hbaseTable =new HbaseTable(hbaseColumn3);
//        hbaseTable.setMeta(false);
//        JSONObject jsonObject = hbaseTable.getHbaseTableInJson();
//        System.out.print(jsonObject);
//        System.out.println(hBaseRestDao.updateHTableByJson("test",jsonObject));

        //删除表
//        System.out.println(hBaseRestDao.deleteHTable("wuzhong"));

        //批量插入数据
//        HbaseCell hbaseCell11=new HbaseCell("family1","column1","11");
//        HbaseCell hbaseCell12=new HbaseCell("family1","column2","12");
//        HbaseCell hbaseCell21=new HbaseCell("family2","column1","21");
//        HbaseCell hbaseCell22=new HbaseCell("family2","column2","22");
//        HbaseRow hbaseRow1=new HbaseRow("row1",hbaseCell11,hbaseCell12,hbaseCell21);
//        HbaseRow hbaseRow2=new HbaseRow("row2",hbaseCell11,hbaseCell22,hbaseCell12,hbaseCell21);
//        HbaseRows hbaseRows=new HbaseRows(hbaseRow1,hbaseRow2);
//        System.out.println(hBaseRestDao.insertRow("test","row1",hbaseRows.getHbaseRowsJson()));

//        long len=0;
//        long allsize=0;
//        long alltime=0;
//
//        File file=new File("C:\\Users\\Administrator\\Desktop\\HBaseData");
//        File[] tempList = file.listFiles();
//        for (int i = 0; i < tempList.length; i++) {
//            if (tempList[i].isFile()) {
//                try {
//                    BufferedReader reader = new BufferedReader(new FileReader(tempList[i]));//换成你的文件名
//                    String line = null;
//                    long size=0;
//                    long time=0;
//                    while ((line=reader.readLine())!=null)
//                    {
//                        Map<String,String> map = new TreeMap<>();
//                        for(int j=0;j!=1000000000;j++)
//                        {
//                            if(j!=0 && (line=reader.readLine())==null) {
//                                break;
//                            }
//                            int index = line.indexOf(',');//CSV格式文件为逗号分隔符文件，这里根据逗号切分
//                            String rowKey = line.substring(0,index);
//                            String value = line.substring(index+1);
//                            map.put(rowKey,value);
//                        }
//                        long startTime = System.currentTimeMillis();
//                        hBaseRestDao.adds("wuzhong",map,"d","k");
//                        long endTime = System.currentTimeMillis();
//                        time += (endTime-startTime);
//                        size += map.size();
//                        allsize += map.size();
//                        alltime += (endTime-startTime);
//                    }
//                    len += tempList[i].length();
//                    System.out.println("插入文件"+tempList[i].getName()+
//                            "总大小:"+tempList[i].length()/1024+"KB共"+size+"条数据,用时"+time+"ms");
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        System.out.println("插入"+len/1024/1024+"MB,共"+allsize+"条数据,用时"+alltime/1000+"s");





        //插入一个单元格
//        System.out.println(hBaseRestDao.insertCell("test","row3","family1","column2","我是yun"));


//        //查找行数据
//        System.out.println(hBaseRestDao.findRow("test","row1"));
//        System.out.println(hBaseRestDao.findRow("test","row2"));

//        //查找列数据
//        System.out.println(hBaseRestDao.findCell("test","row1","family1","column1"));
//        System.out.println(hBaseRestDao.findCell("test","row1","family1","column2"));
//        System.out.println(hBaseRestDao.findCell("test","row1","family2","column1"));
//        System.out.println(hBaseRestDao.findCell("test","row1","family2","column2"));
//        System.out.println(hBaseRestDao.findCell("test","row2","family1","column1"));
//        System.out.println(hBaseRestDao.findCell("test","row2","family1","column2"));
//        System.out.println(hBaseRestDao.findCell("test","row2","family2","column1"));
//        System.out.println(hBaseRestDao.findCell("test","row2","family2","column2"));

//        String s = hBaseRestDao.find("test","row1","family1","column1");
//        System.out.println(s);

    }
}
