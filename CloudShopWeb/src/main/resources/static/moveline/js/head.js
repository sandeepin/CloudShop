/**
 * Created by YY on 2018-01-25.
 */
$("#navigation").moveline({
    color:'#ec6057',
    position:'inner',
    height:60,
    animateType:'easeOutBounce',
    click:function(ret){
        switch (ret.index){
            case 1:
                window.location.href="instances";break;
            case 2:
                window.location.href="pyspark";break;
            case 3:
                window.location.href="spark";break;
            case 4:
                window.location.href="hbase";break;
            case 5: {
                window.location.href="quit";break;
            }
            default:
                window.location.href="main";break;
        }
    }
});