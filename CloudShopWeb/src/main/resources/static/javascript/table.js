function addRow(tableID,jsonData) {
    var tab = document.getElementById(tableID);
    //虚拟机的ID，唯一对应一行
    var vhostID = jsonData.instance_id;
    //新增加一行，并设置该行的id
    var tr = document.createElement("tr");
    tr.setAttribute("id",vhostID);
    //添加第一列为checkbox，id为tr+vhostID+td0
    var td0 = document.createElement("td");
    td0.setAttribute("id",vhostID+"td0");
    var checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.name = 'checkbox';
    td0.appendChild(checkbox);
    tr.appendChild(td0);
    //添加第二列为虚拟机名字和虚拟机的url
    var td1 = document.createElement("td");
    td1.setAttribute("id",vhostID+"td1");
    var a=document.createElement("a");
    a.innerText=jsonData.instance;
    a.href=jsonData.instance_url;
    a.target="_blank";
    td1.appendChild(a);
    tr.appendChild(td1);
    //添加第三列为镜像列
    var td2 = document.createElement("td");
    td2.setAttribute("id",vhostID+"td2");
    td2.innerText=jsonData.image;
    tr.appendChild(td2);
    //添加第四列为ip地址列
    var td3 = document.createElement("td");
    td3.setAttribute("id",vhostID+"td3");
    td3.innerText=jsonData.instance_ip;
    tr.appendChild(td3);
    //添加第五列为虚拟机状态列
    var td4 = document.createElement("td");
    td4.setAttribute("id",vhostID+"td4");
    td4.innerText=jsonData.status;
    tr.appendChild(td4);
    //添加第6列为是否锁定列
    var td5 = document.createElement("td");
    td5.setAttribute("id",vhostID+"td5");
    td5.innerText=jsonData.islocked;
    tr.appendChild(td5);
    //添加第6列为创建时间列
    var td6 = document.createElement("td");
    td6.setAttribute("id",vhostID+"td6");
    td6.innerText=jsonData.instance_time;
    tr.appendChild(td6);
    //添加第7列为操作列
    var td7 = document.createElement("td");
    td7.setAttribute("id",vhostID+"td7");
    var button = document.createElement("button");
    button.setAttribute("id",vhostID+"button");
    button.className="layui-btn";
    button.innerText="ssh连接";
    var options = document.createElement("button");
    options.setAttribute("id",vhostID+"options");
    options.className="layui-btn layui-btn-sm";
    var icon = document.createElement("i");
    icon.innerHTML="&#xe602;";
    icon.className="layui-icon";
    options.appendChild(icon);
    td7.appendChild(options);
    td7.appendChild(button);
    tr.appendChild(td7);
    tab.appendChild(tr);
    button.onclick = function () {
        var image =jsonData.image;
        if(image.toLowerCase().indexOf("ubuntu")>=0){
            os="ubuntu";
        }else if(image.toLowerCase().indexOf("centos")>=0){
            os="centos";
        }else if(image.toLowerCase().indexOf("cirros")>=0) {
            os ="cirros";
        }
        var url = "sshclient?os="+os+"&id="+vhostID;
        window.open(url);
    };
    options.onclick = function() {
        buttonOnClick(tr);
    }
}
//获取鼠标实时位置
var leftX = 0;
var topY = 0;
function mousePosition(ev) {
    if (ev.pageX || ev.pageY) {
        return { x: ev.pageX, y: ev.pageY };
    }
    return {
        x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
        y: ev.clientY + document.body.scrollTop - document.body.clientTop
    };
}
function mouseMove(ev) {
    ev = ev || window.event;
    var mousePos = mousePosition(ev);
    leftX = mousePos.x;
    topY = mousePos.y;
}
document.onmousemove = mouseMove;
//右键方法
function buttonOnClick(rowObject) {
    $("#rMenu").css({
        top: topY + "px",
        left:leftX+"px",
        display: "block",
        "z-index": 1
    });
    showMenu(rowObject,getInstanceAvailableUlId(rowObject));
}
//点击按钮显示菜单
function showMenu(rowObject,array) {
    for (var i = 0; i < array.length; i++) {
        $(array[i]).show();
    }
    $("#rMenu").hover(function () {
        $("#syncUrl").unbind("click");
        $("#syncUrl").click(function () {
            synSingleUrl(rowObject.id);
            $("#rMenu").hide();
        });
        $("#syncStatus").unbind("click");
        $("#syncStatus").click(function () {
            synSingleStatus(rowObject.id);
            $("#rMenu").hide();
        });
        $("#start").unbind("click");
        $("#start").click(function () {
            operateInstance(rowObject,"start");
            $("#rMenu").hide();
        });
        $("#stop").unbind("click");
        $("#stop").click(function () {
            operateInstance(rowObject,"stop");
            $("#rMenu").hide();
        });
        $("#reboot").unbind("click");
        $("#reboot").click(function () {
            operateInstance(rowObject,"reboot");
            $("#rMenu").hide();
        });
        $("#suspend").unbind("click");
        $("#suspend").click(function () {
            operateInstance(rowObject,"suspend");
            $("#rMenu").hide();
        });
        $("#resume").unbind("click");
        $("#resume").click(function () {
            operateInstance(rowObject,"resume");
            $("#rMenu").hide();
        });
        $("#pause").unbind("click");
        $("#pause").click(function () {
            operateInstance(rowObject,"pause");
            $("#rMenu").hide();
        });
        $("#unpause").unbind("click");
        $("#unpause").click(function () {
            operateInstance(rowObject,"unpause");
            $("#rMenu").hide();
        });
        $("#lock").unbind("click");
        $("#lock").click(function () {
            operateInstance(rowObject,"lock");
            $("#rMenu").hide();
        });
        $("#unlock").unbind("click");
        $("#unlock").click(function () {
            operateInstance(rowObject,"unlock");
            $("#rMenu").hide();
        });
        $("#delete").unbind("click");
        $("#delete").click(function () {
            operateInstance(rowObject,"delete");
            $("#rMenu").hide();
        });
    }, function(){
        for (var i = 0; i < array.length; i++) {
            $(array[i]).hide();
        }
    });
}
function getInstanceAvailableUlId(rowObject) {
    return getAvailableOptions(getCurrentStatus(rowObject),getCurrentIsLocked(rowObject));
}
function getCurrentStatus(rowObject) {
    return rowObject.childNodes[4].innerText;
}
function getCurrentIsLocked(rowObject) {
    return rowObject.childNodes[5].innerText;
}
function getAvailableOptions(status,islocked) {
    var array=[];
    if(islocked==="true"){
        array.push("#unlock");
        array.push("#syncUrl");
        array.push("#syncStatus");
        return array;
    }
    if(status==="ACTIVE"){
        array.push("#stop");
        array.push("#reboot");
        array.push("#suspend");
        array.push("#pause");
        array.push("#lock");
    }else if(status==="SHUTDOWN"){
        array.push("#start");
    }else if(status==="SUSPENDED"){
        array.push("#resume");
    }else if(status==="PAUSED"){
        array.push("#unpause");
    }
    array.push("#delete");
    array.push("#syncUrl");
    array.push("#syncStatus");
    return array;
}
//ok
function updateRow(jsonData) {
    var rowID = jsonData.instance_id;
    var td1 = document.getElementById(rowID+"td1");
    var td2 = document.getElementById(rowID+"td2");
    var td3 = document.getElementById(rowID+"td3");
    var td4 = document.getElementById(rowID+"td4");
    var td5 = document.getElementById(rowID+"td5");
    var td6 = document.getElementById(rowID+"td6");
    if(jsonData.instance!==undefined){
        td1.childNodes[0].innerText=jsonData.instance;
    }
    if(jsonData.instance_url!==undefined){
        td1.childNodes[0].href=jsonData.instance_url;
    }
    if(jsonData.image!==undefined){
        td2.innerText=jsonData.image;
    }
    if(jsonData.instance_ip!==undefined){
        td3.innerText=jsonData.instance_ip;;
    }
    if(jsonData.status!==undefined){
        td4.innerText=jsonData.status;
    }
    if(jsonData.islocked!==undefined){
        td5.innerText=jsonData.islocked;
    }
    if(jsonData.instance_time!==undefined){
        td6.innerText=jsonData.instance_time;
    }
}

function deleteRow(tableID,jsonData) {
    layer.msg("成功删除" + jsonData.instance, {
        icon: 6,//哭脸
        time: 2000 //2秒关闭（如果不配置，默认是3秒）
    });
    var rowObject = document.getElementById(jsonData.instance_id);
    document.getElementById(tableID).removeChild(rowObject);
}

function getArrayOfCheckedRow() {
    var checkboxGroup=document.getElementsByName("checkbox");
    // 统计有多少行被选了
    var array=[];
    for(var i=0;i<checkboxGroup.length;++i){
        if(checkboxGroup[i].checked){
            array.push(checkboxGroup[i].parentNode.parentNode);
        }
    }
    return array;
}
function getNameArrayOfCheckedVHost() {
    var checkboxGroup=document.getElementsByName("checkbox");
    // 统计有多少行被选了
    var array=[];
    for(var i=0;i<checkboxGroup.length;++i){
        if(checkboxGroup[i].checked){
            array.push(checkboxGroup[i].parentNode.nextSibling.firstChild.innerText);
        }
    }
    return array;
}
function getRowIdArrayOfCheckedVHost() {
    var checkboxGroup=document.getElementsByName("checkbox");
    // 统计有多少行被选了
    var array=[];
    for(var i=0;i<checkboxGroup.length;++i){
        if(checkboxGroup[i].checked){
            array.push(checkboxGroup[i].parentNode.parentNode.id);
        }
    }
    return array;
}
function selectAllCheckBox() {
    var boxGroup = $("input[name='checkbox']");
    if($("#multi-select").prop("checked")){
        boxGroup.prop("checked",true);
    }else {
        boxGroup.prop("checked", false);
    }
}