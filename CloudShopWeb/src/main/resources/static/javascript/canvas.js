/**
 * Created by YY on 2018-02-01.
 * 用来画图的js文件
 */
function draw4CircleForQuota(json) {
    var use_color = "#ff6e1b";
    var back_color = "#e9e9e9";
    var text_color = "#000";
    var maxInstance = json.maxInstance;
    var maxCpu = json.maxCpu;
    var maxRam = json.maxRam;
    var maxDisk = json.maxDisk;
    var currentInstance = json.currentInstance;
    var currentCpu = json.currentCpu;
    var currentRam = json.currentRam;
    var currentDisk = json.currentDisk;
    var canvasId_array= ["canvas_circle0","canvas_circle1","canvas_circle2","canvas_circle3"];
    drawCircleForQuota(canvasId_array[0],currentInstance,maxInstance,use_color,back_color,text_color,"实例");
    drawCircleForQuota(canvasId_array[1],currentCpu,maxCpu,use_color,back_color,text_color,"VCPU数量");
    drawCircleForQuota(canvasId_array[2],currentRam,maxRam,use_color,back_color,text_color,"内存");
    drawCircleForQuota(canvasId_array[3],currentDisk,maxDisk,use_color,back_color,text_color,"硬盘");
}

function drawCircleForQuota(canvasId, use, all, use_color,back_color,text_color,txt)
{
    var c = document.getElementById(canvasId);
    var ctx = c.getContext("2d");

    var radius = c.height / 2 ; //半径
    var ox =radius , oy = radius; //圆心
    var startAngle = 0; //起始弧度
    var endAngle = 0;   //结束弧度

    if(all==="0"){
        endAngle = Math.PI * 2;
    }else {
        endAngle = endAngle +use/all * Math.PI * 2; //使用配额的结束弧度
    }
    //绘制饼图
    ctx.fillStyle = use_color;
    ctx.beginPath();
    ctx.moveTo(ox, oy); //移动到到圆心
    ctx.arc(ox, oy, radius, startAngle, endAngle, false);
    ctx.closePath();
    ctx.fill();

    ctx.fillStyle = back_color;
    ctx.beginPath();
    ctx.moveTo(ox, oy); //移动到到圆心
    ctx.arc(ox, oy, radius, endAngle, Math.PI * 2, false);
    ctx.closePath();
    ctx.fill();

    var textX = ox+radius;
    var text;
    switch (txt){
        case "实例":{
            ctx.fillStyle = text_color;
            ctx.font = '14px 微软雅黑';
            ctx.fillText(txt,textX+20,oy-20);
            //绘制比例图及文字
            ctx.fillStyle = text_color;
            ctx.font = '14px 微软雅黑';
            text='used '+use+' of '+all;
            ctx.fillText(text, textX+20, oy+20);
        }break;
        case "VCPU数量":{
            ctx.fillStyle = text_color;
            ctx.font = '14px 微软雅黑';
            ctx.fillText(txt,textX+20,oy-20);
            //绘制比例图及文字
            ctx.fillStyle = text_color;
            ctx.font = '14px 微软雅黑';
            text ='used '+use+' of '+all;
            ctx.fillText(text,textX+20, oy+20);
        }break;
        case "内存":{
            ctx.fillStyle = text_color;
            ctx.font = '14px 微软雅黑';
            ctx.fillText(txt,textX+20,oy-20);
            //绘制比例图及文字
            ctx.fillStyle = text_color;
            ctx.font = '14px 微软雅黑';
            text ='used '+(use/1024).toFixed(2)+'GB of '+(all/1024).toFixed(2)+'GB';
            ctx.fillText(text, textX+20, oy+20);
        }break;
        case "硬盘":{
            ctx.fillStyle = text_color;
            ctx.font = '14px 微软雅黑';
            ctx.fillText(txt,textX+20,oy-20);
            //绘制比例图及文字
            ctx.fillStyle = text_color;
            ctx.font = '14px 微软雅黑';
            text ='used '+use+'GB of '+all+'GB';
            ctx.fillText(text, textX+20, oy+20);
        }break;
    }

}

