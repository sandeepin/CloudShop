CREATE DATABASE /*!32312 IF NOT EXISTS*/`cloudshop` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cloudshop`;

DROP TABLE IF EXISTS `openstack_user`;

CREATE TABLE `openstack_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) DEFAULT NULL UNIQUE COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `level` varchar(255) DEFAULT '1' COMMENT '权限',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

insert into `openstack_user`(`id`,`username`,`password`,`level`,`email`,`phone`) values (1,'admin','root','0','1365733349@qq.com','15207135348');
insert into `openstack_user`(`id`,`username`,`password`,`level`,`email`,`phone`) values (2,'yangyun','522578','0','1365733349@qq.com','15207135348');
insert into `openstack_user`(`id`,`username`,`password`,`level`,`email`,`phone`) values (3,'liyinong','1234567','0','992853076@qq.com','17671710823');
insert into `openstack_user`(`id`,`username`,`password`,`level`,`email`,`phone`) values (4,'zc','123456','0','1474266550@qq.com','15207135348');
insert into `openstack_user`(`id`,`username`,`password`,`level`,`email`,`phone`) values (5,'yl','yl123456','0','2814827816@qq.com','17671710823');

DROP TABLE IF EXISTS `openstack_vhost`;

CREATE TABLE `openstack_vhost`(
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `hostname` varchar(255) DEFAULT NULL COMMENT '虚拟机名',
  `hostid` varchar(255) DEFAULT NULL COMMENT '虚拟机ID',
  `imagename` varchar(255) DEFAULT NULL COMMENT '镜像名',
  `cpu` varchar(255) DEFAULT NULL COMMENT 'cpu数量',
  `ram` varchar(255) DEFAULT NULL COMMENT '内存',
  `disk` varchar(255) DEFAULT NULL COMMENT '硬盘',
  `ip` varchar(255) DEFAULT NULL UNIQUE COMMENT 'ip地址',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `createtime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `islocked` varchar (255) DEFAULT NULL COMMENT '是否锁定',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
insert into `openstack_vhost`(`id`,`username`,`hostname`,`hostid`,`imagename`,`cpu`,`ram`,`disk`,`ip`,`url`,`createtime`,`islocked`,`status`) values (1,'yangyun','CloudShop','0bf01ccf-fc3f-4ea2-b00c-b91621ae878a','ubuntu16.04.server','8','9000','256','provider=192.168.1.39','http://59.69.101.206:6080/vnc_auto.html?token=856167c6-8e69-480a-9c8d-a63fcee4caf5','2018-01-03 09:04','false','ACTIVE');
insert into `openstack_vhost`(`id`,`username`,`hostname`,`hostid`,`imagename`,`cpu`,`ram`,`disk`,`ip`,`url`,`createtime`,`islocked`,`status`) values (2,'liyinong','test','110d6805-77f6-4050-b21f-194e4e84979c','ubuntu16.04.server','2','1024','120','provider=192.168.1.42','http://59.69.101.206:6080/vnc_auto.html?token=050cb0ba-3328-48cd-b8e9-80a83a13ca11','2018-01-15 03:09:04','false','ACTIVE');
insert into `openstack_vhost`(`id`,`username`,`hostname`,`hostid`,`imagename`,`cpu`,`ram`,`disk`,`ip`,`url`,`createtime`,`islocked`,`status`) values (3,'yl','xuya','8f9afc97-4694-4cb7-aaf9-8703524eec91','ubuntu16.04.server','4','4096','128','provider=192.168.1.35','http://59.69.101.206:6080/vnc_auto.html?token=050cb0ba-3328-48cd-b8e9-80a83a13ca11','2017-12-29 08:04','false','ACTIVE');
insert into `openstack_vhost`(`id`,`username`,`hostname`,`hostid`,`imagename`,`cpu`,`ram`,`disk`,`ip`,`url`,`createtime`,`islocked`,`status`) values (4,'yl','jjing','cb996305-87d9-4bfa-910b-c2c51b03119f','ubuntu16.04.server','4','4096','128','provider=192.168.1.38','http://59.69.101.206:6080/vnc_auto.html?token=050cb0ba-3328-48cd-b8e9-80a83a13ca11','2017-12-29 10:51','false','ACTIVE');
insert into `openstack_vhost`(`id`,`username`,`hostname`,`hostid`,`imagename`,`cpu`,`ram`,`disk`,`ip`,`url`,`createtime`,`islocked`,`status`) values (5,'yl','taoyy','dc39cd39-caf8-4f09-a18f-603225ccc374','ubuntu16.04.server','4','4096','128','provider=192.168.1.34','http://59.69.101.206:6080/vnc_auto.html?token=050cb0ba-3328-48cd-b8e9-80a83a13ca11','2017-12-29 10:51','false','ACTIVE');
insert into `openstack_vhost`(`id`,`username`,`hostname`,`hostid`,`imagename`,`cpu`,`ram`,`disk`,`ip`,`url`,`createtime`,`islocked`,`status`) values (6,'yl','yangliu','c484c273-8277-4283-b998-2131774fd10d','ubuntu16.04.server','4','4096','128','provider=192.168.1.43','http://59.69.101.206:6080/vnc_auto.html?token=050cb0ba-3328-48cd-b8e9-80a83a13ca11','2017-12-29 10:54','false','ACTIVE');
insert into `openstack_vhost`(`id`,`username`,`hostname`,`hostid`,`imagename`,`cpu`,`ram`,`disk`,`ip`,`url`,`createtime`,`islocked`,`status`) values (7,'zc','cmfg','07b42328-a9ce-4e3a-a9ee-a3fc0ae7cb5a','ubuntu16.04.server','4','4096','128','provider=192.168.1.46','http://59.69.101.206:6080/vnc_auto.html?token=050cb0ba-3328-48cd-b8e9-80a83a13ca11','2018-01-05 10:38','false','ACTIVE');

DROP TABLE IF EXISTS `openstack_user_quota`;

CREATE TABLE `openstack_user_quota`(
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) DEFAULT NULL UNIQUE COMMENT '用户名',
  `maxinstance` varchar(255) DEFAULT NULL COMMENT '虚拟机数量上限',
  `maxcpu` varchar(255) DEFAULT NULL COMMENT 'cpu数量上限',
  `maxram` varchar(255) DEFAULT NULL COMMENT '内存大小上限',
  `maxdisk` varchar(255) DEFAULT NULL COMMENT '硬盘大小上限',
  `currentinstance` varchar(255) DEFAULT NULL COMMENT '当前虚拟机数量',
  `currentcpu` varchar(255) DEFAULT NULL COMMENT '当前使用cpu数量',
  `currentram` varchar(255) DEFAULT NULL COMMENT '当前使用内存大小',
  `currentdisk` varchar(255) DEFAULT NULL COMMENT '当前使用硬盘大小',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
insert into `openstack_user_quota`(`id`,`username`,`maxinstance`,`maxcpu`,`maxram`,`maxdisk`,`currentinstance`,`currentcpu`,`currentram`,`currentdisk`) values (1,'admin','0','0','0','0','0','0','0','0');
insert into `openstack_user_quota`(`id`,`username`,`maxinstance`,`maxcpu`,`maxram`,`maxdisk`,`currentinstance`,`currentcpu`,`currentram`,`currentdisk`) values (2,'yangyun','2','20','20000','1000','1','16','16384','256');
insert into `openstack_user_quota`(`id`,`username`,`maxinstance`,`maxcpu`,`maxram`,`maxdisk`,`currentinstance`,`currentcpu`,`currentram`,`currentdisk`) values (3,'liyinong','2','4','2048','240','1','2','1024','120');
insert into `openstack_user_quota`(`id`,`username`,`maxinstance`,`maxcpu`,`maxram`,`maxdisk`,`currentinstance`,`currentcpu`,`currentram`,`currentdisk`) values (4,'yl','4','16','16384','512','4','16','16384','512');
insert into `openstack_user_quota`(`id`,`username`,`maxinstance`,`maxcpu`,`maxram`,`maxdisk`,`currentinstance`,`currentcpu`,`currentram`,`currentdisk`) values (5,'zc','1','4','4096','128','1','4','4096','128');
