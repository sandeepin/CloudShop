//$(document).ready(function(){
//    InitLeftMenu();
//    tabClose();
//    tabCloseEven();
//});

//初始化左侧
function InitLeftMenu() {
    $('.nav nav-list li ul li a').click(function(){
        var tabTitle = $(this).text();
        var url = $(this).attr("rel");
        addTab(tabTitle,url);
        $('.navlist li div').removeClass("selected");
        $(this).parent().addClass("selected");
    }).hover(function(){
        $(this).parent().addClass("hover");
    },function(){
        $(this).parent().removeClass("hover");
    });
}

function addTab(subtitle,url){
    if(!$('#tabs').tabs('exists',subtitle)){
        $('#tabs').tabs('add',{
            title:subtitle,
            content:createFrame(url),
            closable:true
        });
    }else{
        $('#tabs').tabs('select',subtitle);
    }
    tabClose();
}

function isExists(subtitle){
	var bool=false;
	bool=$('#tabs').tabs('exists',subtitle);
	return bool;
}

function refreshTab(subtitle,url){
	$('#tabs').tabs('select', subtitle);
	var currentTab =$('#tabs').tabs('getSelected');
    $('#tabs').tabs('update', {
        tab: currentTab,
        options: {
            content: createFrame(url)
        }
    });
}

function updateTab() {
	var currentTab = $('#tabs').tabs('getSelected');
	var iframe = $(currentTab.panel('options').content);
	var src = iframe.attr('src');
	$('#tabs').tabs('update', {
		tab : currentTab,
		options : {
			content : createFrame(src)
		}
	});
}

function createFrame(url){
    var s = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
    return s;
}

function tabClose(){
    /*双击关闭TAB选项卡*/
    $(".tabs-inner").dblclick(function(){
        var subtitle = $(this).children(".tabs-closable").text();
        $('#tabs').tabs('close',subtitle);
    })
    /*为选项卡绑定右键*/
    $(".tabs-inner").bind('contextmenu',function(e){
        $('#mm').menu('show', {
            left: e.pageX,
            top: e.pageY
        });
        var subtitle =$(this).children(".tabs-closable").text();
        $('#mm').data("currtab",subtitle);
        $('#tabs').tabs('select',subtitle);
        return false;
    });
}

//绑定右键菜单事件
function tabCloseEven() {
    $('#mm').menu({
        onClick: function (item) {
            closeTab(item.id);
        }
    });
    return false;
}

function closeTab(action){
	var onlyOpenTitle="欢迎使用";
    var alltabs = $('#tabs').tabs('tabs');
    var currentTab =$('#tabs').tabs('getSelected');
    var allTabtitle = [];
    $.each(alltabs,function(i,n){
        allTabtitle.push($(n).panel('options').title);
    })
    switch (action) {
        case "refresh":
            var iframe = $(currentTab.panel('options').content);
            if(currentTab.panel('options').title==onlyOpenTitle){
            	return;
            }
            var src = iframe.attr('src');
            $('#tabs').tabs('update', {
                tab: currentTab,
                options: {
                    content: createFrame(src)
                }
            });
            break;
        case "close":
            var currtab_title = currentTab.panel('options').title;
            $('#tabs').tabs('close', currtab_title);
            break;
        case "closeall":
            $.each(allTabtitle, function (i, n) {
                if (n != onlyOpenTitle){
                    $('#tabs').tabs('close', n);
                }
            });
            break;
        case "closeother":
            var currtab_title = currentTab.panel('options').title;
            $.each(allTabtitle, function (i, n) {
                if (n != currtab_title && n != onlyOpenTitle){
                    $('#tabs').tabs('close', n);
                }
            });
            break;
        case "index":
        	var local = window.location;  
        	var contextPath = local.pathname.split("/")[1];  
            var basePath = local.protocol+"//"+local.host+"/"+contextPath+"/";
        	/*var basePath = local.protocol+"//"+local.host+"/";  */
            window.open(basePath);
            break;
    }
}