package com.whut.util;

import java.util.Properties;

/**
 * Created by YY on 2018-01-21.
 */
public class OSUtil {

    private static String getOsName()
    {
        Properties prop = System.getProperties();
        return prop.getProperty("os.name").toLowerCase();
    }

    public static boolean isLinux() {

        if (getOsName().indexOf("linux") > -1) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean isWindow(){
        if (getOsName().indexOf("windows") > -1) {
            return true;
        } else {
            return false;
        }
    }
}
