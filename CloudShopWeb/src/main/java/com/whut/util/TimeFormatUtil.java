package com.whut.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by YY on 2018-01-14.
 */
public class TimeFormatUtil {

    public static String getCurrentTime(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        return df.format(new Date());// new Date()为获取当前系统时间
    }
}
