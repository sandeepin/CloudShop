package com.whut.util;

import org.springframework.messaging.MessagingException;

import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * @author Sandeepin
 * 数据中心断开后发邮件给管理员
 * 2017/3/31
 */
public class MailUtil {
    public static void sendMail(String text) throws MessagingException, javax.mail.MessagingException {
        String title = "【云平台信息】数据中心断开!";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String curreTime = df.format(new Date());
        String textAll = "<a href='http://59.69.101.206/WhutMonitor/historysearch/findori?date=" + curreTime + "&idArray=001-SCD-00'>" + text + "</a>";
        sendMailCore(textAll, title, "smtp.sina.com", "whutifo@sina.com", "whut1234", "1427582780@qq.com", "sandeepin@qq.com");
        sendMailCore(textAll, title, "smtp.sina.com", "whutcloud@sina.com", "whut123456", "1135385536@qq.com", "1365733349@qq.com");
        sendMailCore(textAll, title, "smtp.aliyun.com", "whutifo@aliyun.com", "whut1234", "710229096@qq.com", "543424911@qq.com");
    }

    public static void sendMail(String text, String title, String email) throws MessagingException, javax.mail.MessagingException {
        sendMailCore(text, title, "smtp.aliyun.com", "whutifo@aliyun.com", "whut1234", email, null);
    }

    public static void sendMail(String text, String title, String email1, String email2, String email3, String email4) throws MessagingException, javax.mail.MessagingException {
        sendMailCore(text, title, "smtp.aliyun.com", "whutifo@aliyun.com", "whut1234", email1, email2);
        sendMailCore(text, title, "smtp.sina.com", "whutifo@sina.com", "whut1234", email3, "1261201074@qq.com");
        sendMailCore(text, title, "smtp.aliyun.com", "yangyun130030@aliyun.com", "36232219940513yy", email4, "517909395@qq.com");
        sendMailCore(text, title, "smtp.sina.com", "whutcloud@sina.com", "whut123456", "1286163697@qq.com", "sandeepin@qq.com");
    }

    public static void sendMail(String text, String title, String email, int provider) throws MessagingException, javax.mail.MessagingException {
        switch (provider){
            case 1:
                sendMailCore(text, title, "smtp.aliyun.com", "whutifo@aliyun.com", "whut1234", email, "sandeepin@qq.com");
                break;
            case 2:
                sendMailCore(text, title, "smtp.sina.com", "whutifo@sina.com", "whut1234", email, "sandeepin@qq.com");
                break;
            case 3:
                sendMailCore(text, title, "smtp.sina.com", "whutcloud@sina.com", "whut123456", email, "sandeepin@qq.com");
                break;
            default:
                sendMailCore(text, title, "smtp.aliyun.com", "whutifo@aliyun.com", "whut1234", email, "sandeepin@qq.com");
        }
    }

    public static void sendMailCore(String text, String title, String host, String user, String password, String address1, String address2) throws MessagingException, javax.mail.MessagingException {
        // 配置发送邮件的环境属性
        final Properties props = new Properties();
        // 表示SMTP发送邮件，需要进行身份验证
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", host);
        // 发件人的账号
        props.put("mail.user", user);
        // 访问SMTP服务时需要提供的密码
        props.put("mail.password", password);
        // 构建授权信息，用于进行SMTP进行身份验证
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                // 用户名、密码
                String userName = props.getProperty("mail.user");
                String password = props.getProperty("mail.password");
                return new PasswordAuthentication(userName, password);
            }
        };

        // 使用环境属性和授权信息，创建邮件会话
        Session mailSession = Session.getInstance(props, authenticator);
        // 创建邮件消息
        MimeMessage message = new MimeMessage(mailSession);
        // 设置发件人
        InternetAddress form = new InternetAddress(props.getProperty("mail.user"));
        message.setFrom(form);
        // 设置收件人
        InternetAddress to = new InternetAddress(address1);
        message.setRecipient(MimeMessage.RecipientType.TO, to);
        // 设置抄送
        // InternetAddress	cc	  =	new	InternetAddress("1365733349@qq.com");
        // message.setRecipient(RecipientType.CC, cc);
        // 设置密送，其他的收件人不能看到密送的邮件地址
        if (address2 != null){
            InternetAddress bcc = new InternetAddress(address2);
            message.setRecipient(MimeMessage.RecipientType.CC, bcc);
        }
        // 设置邮件标题
        message.setSubject(title);
        // 设置邮件的内容体
        message.setContent(text, "text/html;charset=UTF-8");
        // 发送邮件
        Transport.send(message);
    }
}
