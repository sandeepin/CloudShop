package com.whut.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Sandeepin
 *         2017/11/11 0011
 */
public class DateUtil {

    // 获取时间段列表 自定义间隔
    public static String[] getTimeList(String start, String end, int deltaSecond) throws ParseException {
        start = "20" + start;
        end   =  "20" + end;
        deltaSecond = deltaSecond*1000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date startDate = sdf.parse(start);
        Date endDate = sdf.parse(end);
        long startLong = startDate.getTime();
        long endLong = endDate.getTime();
        List<Long> dateLongList = new ArrayList<>();
        for (long i = startLong; i < endLong; i+=deltaSecond) {
            dateLongList.add(i);
        }
        String[] dateStrArray = new String[dateLongList.size()];
        for (int i = 0; i < dateLongList.size(); ++i) {
            dateStrArray[i] = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date(dateLongList.get(i)));;
        }
        return dateStrArray;
    }

    // 获取时间段列表 间隔60s
    public static String[] getTimeList60s(String start, String end) throws ParseException {
        start = "20" + start;
        end   =  "20" + end;
        int deltaSecond = 60*1000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date startDate = sdf.parse(start);
        Date endDate = sdf.parse(end);
        long startLong = startDate.getTime();
        long endLong = endDate.getTime();
        List<Long> dateLongList = new ArrayList<>();
        for (long i = startLong; i < endLong; i+=deltaSecond) {
            dateLongList.add(i);
        }
        String[] dateStrArray = new String[dateLongList.size()];
        for (int i = 0; i < dateLongList.size(); ++i) {
            dateStrArray[i] = new SimpleDateFormat("yyyy-MM-dd-HH:mm:00").format(new Date(dateLongList.get(i)));;
        }
        return dateStrArray;
    }

    // 获取时间段列表 间隔10s
    public static String[] getTimeList10s(String start, String end) throws ParseException {
        start = "20" + start;
        end   =  "20" + end;
        int deltaSecond = 10*1000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date startDate = sdf.parse(start);
        Date endDate = sdf.parse(end);
        long startLong = startDate.getTime();
        long endLong = endDate.getTime();
        List<Long> dateLongList = new ArrayList<>();
        for (long i = startLong; i < endLong; i+=deltaSecond) {
            dateLongList.add(i);
        }
        String[] dateStrArray = new String[dateLongList.size()];
        for (int i = 0; i < dateLongList.size(); ++i) {
            dateStrArray[i] = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date(dateLongList.get(i)));
            dateStrArray[i] = dateStrArray[i].substring(0,dateStrArray[i].length()-1) + "0";
        }
        return dateStrArray;
    }

    public static String getNowDate() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(new Date());
    }

    public static String getNowTime() {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(new Date());
    }

    public static String getNowTimeS() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(new Date());
    }

    public static String getNowTimeMS() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return df.format(new Date());
    }

    public static String getNowHbaseTimeS() {
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");
        return df.format(new Date());
    }

    // 转Hbase日期 只有日期
    public static String getHbaseDate(String date) {
        try {
            Date timestand = (new SimpleDateFormat("yyyy-MM-dd")).parse(date);
            return (new SimpleDateFormat("yyMMdd")).format(timestand);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "000000";
    }

    // 转Hbase时间 只有时间
    public static String getHbaseTime(String time) {
        try {
            Date timestand = (new SimpleDateFormat("HH:mm:ss")).parse(time);
            return (new SimpleDateFormat("HHmmss")).format(timestand);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "000000";
    }

    // 转Hbase时间 精确到秒
    public static String getHbaseTimeS(String time) {
        try {
            Date timestand = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(time);
            return (new SimpleDateFormat("yyMMddHHmmss")).format(timestand);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "000000000000";
    }

    // 转Hbase时间 精确到分
    public static String getHbaseTimeM(String time) {
        try {
            Date timestand = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(time);
            return (new SimpleDateFormat("yyMMddHHmm")).format(timestand);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "0000000000";
    }

    // Hbase转标准时间 精确到秒
    public static String getTimeS(String time) {
        try {
            Date timestand = (new SimpleDateFormat("yyMMddHHmmss")).parse(time);
            return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(timestand);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "0000-00-00 00:00:00";
    }

    public static void main(String[] args) throws ParseException {
//        System.out.println(getTimeS("171112131415"));
//        System.out.println(getHbaseTime("12:13:14"));
//        System.out.println(getHbaseTimeS("2017-09-10 14:13:12"));
//        System.out.println(getHbaseTimeM("2017-09-10 14:13:12"));
//        System.out.println(getNowDate());
//        System.out.println(getNowTime());
//        System.out.println(getNowTimeS());
//        System.out.println(getNowTimeMS());
//        System.out.println(getNowHbaseTime());
//        String a = ",,,,";
//        String[] aa = a.split(",",-1);
//        int b = aa.length;
//        if("".equals(aa[0])) {
//            System.out.println("000" + b);
//        }
//
//        String endTime = "171121070000";
//        System.out.println("原来的：" + endTime);
//        int deltaSecond = 86400*1000;
//        endTime = "20" + endTime;
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//        Date endDate = sdf.parse(endTime);
//        long endLong = endDate.getTime();
//        endLong += deltaSecond;
//        endTime = new SimpleDateFormat("yyMMddHHmmss").format(new Date(endLong));
//        System.out.println("加一天：" + endTime);
    }
}
