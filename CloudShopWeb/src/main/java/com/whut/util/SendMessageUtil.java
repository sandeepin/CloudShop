package com.whut.util;

import com.alibaba.fastjson.JSON;
import com.whut.wesocket.PollWebSocketServlet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sandeepin
 * 将信息封装为Json格式传给前台
 */
public class SendMessageUtil {
	public static void sendMessage(String strDate, String monitorNum, String value, PollWebSocketServlet webSocketServlet) throws ParseException {
		Map<String, Object> map = new HashMap<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = sdf.parse(strDate);
		map.put("success", true);
		map.put("num", monitorNum);
		map.put("value", value);
		map.put("date", date);
		String json = JSON.toJSONString(map);
		webSocketServlet.sendMes(monitorNum, json);
	}
	public static void sendMessage(String websocketKey, String value, PollWebSocketServlet webSocketServlet) throws ParseException {
		Map<String, Object> map = new HashMap<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(new Date());
		map.put("success", true);
		map.put("num", websocketKey);
		map.put("value", value);
		map.put("date", date);
		String json = JSON.toJSONString(map);
		webSocketServlet.sendMes(websocketKey, json);
	}
	public static void sendMessage(Date dateNow, String monitorNum, String value, PollWebSocketServlet webSocketServlet) throws ParseException {
		Map<String, Object> map = new HashMap<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String date = formatter.format(dateNow);
		map.put("success", true);
		map.put("num", monitorNum);
		map.put("value", value);
		map.put("date", date);
		String json = JSON.toJSONString(map);
		webSocketServlet.sendMes(monitorNum, json);
	}
	public static void sendMessage(String monitorNum, Map<String,String> temMap, PollWebSocketServlet webSocketServlet) throws ParseException {
		String json = JSON.toJSONString(temMap);
		webSocketServlet.sendMes(monitorNum, json);
	}
	public static void sendMessageByWsj(String monitorNum, String content, PollWebSocketServlet webSocketServlet) throws ParseException {
		webSocketServlet.sendMes(monitorNum, content);
	}
}
