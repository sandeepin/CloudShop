package com.whut.util;

import com.whut.wesocket.PollWebSocketServlet;
import org.springframework.messaging.MessagingException;

import java.io.File;
import java.text.ParseException;

import static com.whut.util.MailUtil.sendMail;
import static com.whut.util.SshUtil.sparkCompute;


/**
 * 用于线程计算
 *
 * @author Sandeepin
 *         2017/11/30 0030
 */
public class ThreadUtil extends Thread {
    private Thread t;
    private String threadName;

    private String filePath;
    private String fileName;
    private String md5value;
    private String email;
    private PollWebSocketServlet webSocketServlet;

    public ThreadUtil(String threadName, String filePath, String fileName, String md5value, String email, PollWebSocketServlet webSocketServlet) {
        this.threadName = threadName;
        this.filePath = filePath;
        this.fileName = fileName;
        this.md5value = md5value;
        this.email = email;
        this.webSocketServlet = webSocketServlet;
        System.out.println("创建线程 " + threadName);
    }

    @Override
    public void run() {
        System.out.println("线程运行 " + threadName);
        long startTime = System.currentTimeMillis();
        try {
            SendMessageUtil.sendMessage(md5value, "后台提交计算任务...", webSocketServlet);
            String modelDownloadLink = sparkCompute(filePath, fileName, md5value, email, webSocketServlet);
            String[] statusAndLink = modelDownloadLink.split("#");
            // 检测SSH返回无错
            if ("0".equals(statusAndLink[0])) {
                String cost = (System.currentTimeMillis() - startTime)/1000.0 + "秒";
                // 检测生成模型文件了且大于2KB
                File f = new File(filePath + "model.zip");
                if (f.exists() && f.isFile() && f.length() / 1000 > 2) {
                    System.out.println(f.length() / 1000 + "KB");

                    String title = "Spark模型" + md5value + "【下载】";
                    String text = "生成模型成功！耗时" + cost + "，下载地址为：" + statusAndLink[1];
                    sendMail(text, title, email);
                    SendMessageUtil.sendMessage(md5value, "耗时" + cost + "，请检查邮件以获取下载地址！", webSocketServlet);
                } else {
//                    String title = "Spark模型" + md5value + "【太小】";
//                    String text = "生成模型太小，不提供下载地址！请用户检查代码，如果你的代码仅显示结果，请忽略此邮件。";
//                    sendMail(text, title, email);
                    SendMessageUtil.sendMessage(md5value, "耗时" + cost + "！", webSocketServlet);
                    SendMessageUtil.sendMessage(md5value, "生成模型文件太小，请用户检查代码并严格按照流程操作，如果你的代码仅显示结果，请忽略。", webSocketServlet);
                }
            }
        } catch (MessagingException e) {
            System.out.println("线程 " + threadName + " MessagingException");
            e.printStackTrace();
        } catch (ParseException e) {
            System.out.println("线程 " + threadName + " ParseException");
            e.printStackTrace();
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
        System.out.println("线程 " + threadName + " 退出.");
    }

    @Override
    public void start() {
        System.out.println("开始线程 " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}
