package com.whut.util;

import com.whut.domain.CreateParameter;
import com.whut.domain.Parameter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by YY on 2018-01-31.
 */
public class JsonUtil {
    public static Parameter[] JsonArray2ParameterArray(String username,String password,JSONArray jsonArray){
        Parameter[] array=new Parameter[jsonArray.length()];
        for(int i=0;i!=jsonArray.length();++i){
            String instance_id= null;
            try {
                instance_id = jsonArray.getString(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            array[i]= new Parameter(username,password,instance_id);
        }
        return array;
    }

    public static CreateParameter[] JsonArray2CreateParameterArray(String username, String password, JSONArray jsonArray){
        CreateParameter[] array=new CreateParameter[jsonArray.length()];
        for(int i=0;i!=jsonArray.length();++i){
            JSONObject object= null;
            try {
                object = jsonArray.getJSONObject(i);
                array[i]= new CreateParameter(username,password,object.get("instance").toString(),object.get("image").toString(),
                        object.get("vcpu").toString(),object.get("ram").toString(),object.get("disk").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return array;
    }
}
