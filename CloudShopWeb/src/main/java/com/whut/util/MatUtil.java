package com.whut.util;

import java.util.Map;

/**
 * @author Sandeepin
 *         2017/11/8 0008
 */
public class MatUtil {

    /**
     * 矩阵转置 double版
     *
     * @param inputMat 输入
     * @return 输出
     */
    public static double[][] transpose(double[][] inputMat) {
        int row = inputMat.length;
        int col;
        if (row == 0) {
            return null;
        }
        col = inputMat[0].length;
        double[][] outputMat = new double[col][row];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                outputMat[j][i] = inputMat[i][j];
            }
        }
        return outputMat;
    }

    /**
     * 矩阵转置 String版
     *
     * @param inputMat 输入
     * @return 输出
     */
    public static String[][] transpose(String[][] inputMat) {
        int row = inputMat.length;
        int col;
        if (row == 0) {
            return null;
        }
        col = inputMat[0].length;
        String[][] outputMat = new String[col][row];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                outputMat[j][i] = inputMat[i][j];
            }
        }
        return outputMat;
    }

    /**
     * 合并矩阵，左右合并，用户确保矩阵非空
     *
     * @param leftMat  左矩阵
     * @param rightMat 右矩阵
     * @return 合并矩阵
     */
    public static String[][] combineRight(String[][] leftMat, String[][] rightMat) {
        int upMatRow = leftMat.length;
        int upMatCol = leftMat[0].length;
        int downMatRow = rightMat.length;
        int downMatCol = rightMat[0].length;

        int row = upMatRow < downMatRow ? upMatRow : downMatRow;
        int col = upMatCol + downMatCol;

        String[][] outMat = new String[row][col];
        int colCnt = 0;
        for (int j = 0; j < upMatCol; j++) {
            for (int i = 0; i < row; i++) {
                outMat[i][colCnt] = leftMat[i][j];
            }
            colCnt++;
        }
        for (int j = 0; j < downMatCol; j++) {
            for (int i = 0; i < row; i++) {
                outMat[i][colCnt] = rightMat[i][j];
            }
            colCnt++;
        }
        return outMat;
    }

    /**
     * 合并矩阵，上下合并，用户确保矩阵非空
     *
     * @param upMat   上矩阵
     * @param downMat 下矩阵
     * @return 合并矩阵
     */
    public static String[][] combineDown(String[][] upMat, String[][] downMat) {
        int upMatRow = upMat.length;
        int upMatCol = upMat[0].length;
        int downMatRow = downMat.length;
        int downMatCol = downMat[0].length;

        int row = upMatRow + downMatRow;
        int col = upMatCol < downMatCol ? upMatCol : downMatCol;

        String[][] outMat = new String[row][col];
        int rowCnt = 0;
        for (int i = 0; i < upMatRow; i++) {
            for (int j = 0; j < col; j++) {
                outMat[rowCnt][j] = upMat[i][j];
            }
            rowCnt++;
        }
        for (int i = 0; i < downMatRow; i++) {
            for (int j = 0; j < col; j++) {
                outMat[rowCnt][j] = downMat[i][j];
            }
            rowCnt++;
        }
        return outMat;
    }

    /**
     * Map的值合并，返回合并的Map Value累加到mapRight上（方便递归）
     * 【默认Map条数相同，Key相同才能用本函数】
     *
     * @param mapLeft 左边的Map
     * @param mapRight 右边的Map
     */
    public static void combineMapValue(Map<String, String> mapLeft, Map<String, String> mapRight){
        if (mapLeft.size() == mapRight.size()){
            for (String akey: mapRight.keySet()) {
                String valueLeft = mapLeft.get(akey);
                String valueRight = mapRight.get(akey);
                String value = valueLeft + valueRight;
                mapRight.put(akey, value);
            }
        }
    }

//    public static void main(String[] args) {
//        String[][] a = {{"1", "2", "3"}, {"4", "5", "6"}};
//        String[][] b = {{"7", "8", "9"}, {"10", "11", "12"}};
//
//        show(transpose(a));
//
//        show(combineRight(a, b));
//        show(combineDown(a, b));
//        System.out.println("MatUtil Test OK!");
//    }
}
