package com.whut.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;

/**
 * 主要页面映射
 * @author Sandeepin
 */
@Controller
public class IndexController {

    // 登陆页面不拦截
    @RequestMapping("/login")
    public String login(HttpServletRequest request) {
        return "login";
    }
    // 注册页面不拦截
    @RequestMapping("/register")
    public String register(HttpServletRequest request) {
        return "register";
    }
    // 退出不拦截
    @RequestMapping("/quit")
    public String loginOut(HttpServletRequest request) {
        request.getSession().invalidate();
        return "login";
    }
    // 云平台主页面拦截
    @RequestMapping("/main")
    public String main(HttpServletRequest request) {
        if(request.getSession().getAttribute("user")!=null) {
            return "main";
        }
        return "login";
    }
    /**
     * 云主机页面
     * @return 页面
     */
    @RequestMapping("/instances")
    public String instances(HttpServletRequest request){
        if(request.getSession().getAttribute("user")!=null) {
            return "instances";
        }
        return "login";
    }
    /**
     * 云存储页面
     * @return 页面
     */
    @RequestMapping("/hbase")
    public String hBase(HttpServletRequest request){

        if(request.getSession().getAttribute("user")!=null)
        {
            return "hbase";
        }
        return "login";
    }
    /**
     * 云计算页面
     * @return 页面
     */
    @RequestMapping("/pyspark")
    public String pyspark(HttpServletRequest request){
        if(request.getSession().getAttribute("user")!=null)
        {
            return "pyspark";
        }
        return "login";
    }
    /**
     * 云计算页面
     * @return 页面
     */
    @RequestMapping("/spark")
    public String spark(HttpServletRequest request){
        if(request.getSession().getAttribute("user")!=null)
        {
            return "spark";
        }
        return "login";
    }
    @RequestMapping("/instanceform")
    public String instanceform(HttpServletRequest request){
        if(request.getSession().getAttribute("user")!=null)
        {
            return "instanceform";
        }
        return "login";
    }
    @RequestMapping("/quotaform")
    public String quotaform(HttpServletRequest request){
        if(request.getSession().getAttribute("user")!=null)
        {
            return "quotaform";
        }
        return "login";
    }
    @RequestMapping("/changequotaform")
    public String changquotaform(HttpServletRequest request){
        if(request.getSession().getAttribute("user")!=null) {
            return "changequotaform";
        }
        return "login";
    }
    @RequestMapping("/sshclient")
    public String sshclient(HttpServletRequest request){
        if(request.getSession().getAttribute("user")!=null) {
            return "sshclient";
        }
        return "login";
    }

}
