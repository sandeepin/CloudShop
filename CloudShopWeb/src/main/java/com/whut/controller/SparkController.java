package com.whut.controller;

import com.whut.domain.SparkUser;
import com.whut.domain.User;
import com.whut.service.SparkService;
import com.whut.util.SendMessageUtil;
import com.whut.wesocket.PollWebSocketServlet;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Map;
import java.util.TreeMap;

import static com.whut.util.DateUtil.getNowTimeMS;
import static com.whut.util.SystemUtil.getMd5;

/**
 * Created by YY on 2018-01-21.
 */
@Controller
@RequestMapping("/spark")
public class SparkController {
    private static final Logger logger = Logger.getLogger(SparkController.class);
    private static Map<String,String> md5Map=new TreeMap<>();
    private final
    SparkService sparkService;
    private final
    PollWebSocketServlet pollWebSocketServlet;

    @Autowired
    public SparkController(SparkService sparkService, PollWebSocketServlet pollWebSocketServlet) {
        this.sparkService = sparkService;
        this.pollWebSocketServlet = pollWebSocketServlet;
    }

    /**
     * 获取MD5的前8位
     *
     * @param request HttpServletRequest
     * @return md5
     * @throws NoSuchAlgorithmException 异常
     */
    @RequestMapping("/getSimpleMd5")
    @ResponseBody
    public String getSimpleMd5(HttpServletRequest request) throws NoSuchAlgorithmException {
        User user = (User)request.getSession().getAttribute("user");
        String md5str="";
        if(user!=null) {
            String username = user.getUserName();
            md5str = getMd5(getNowTimeMS() + request.getSession().toString()).substring(0,8);
            md5Map.put(username+"/spark",md5str);
        }
        return md5str;
    }
    //ok
    @PostMapping("/createSparkUser")
    @ResponseBody //表示返回的是数据
    int createSparkUser(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            return sparkService.createSparkUser(username);
        }
        return 404;//没有认证
    }
    //ok
    @PostMapping("/deleteSparkUser")
    @ResponseBody //表示返回的是数据
    boolean deleteSparkUser(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            return sparkService.deleteSparkUser(username);
        }
        return false;
    }
    //ok
    @PostMapping("/openAllProject")
    @ResponseBody //表示返回的是数据
    String openAllProject(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String json = sparkService.getAllProjectSchema(username).toString();
            logger.info(json);
            return json;
        }
        return null;
    }
    //ok
    @PostMapping("/createProject")
    @ResponseBody //表示返回的是数据
    int createProject(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String projectName=request.getParameter("projectName");
            return sparkService.createProject(username,projectName);
        }
        return 404;//没有认证
    }
    //ok
    @PostMapping("/deleteProject")
    @ResponseBody //表示返回的是数据
    boolean deleteProject(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String projectName=request.getParameter("projectName");
            return sparkService.deleteProject(username,projectName);
        }
        return false;
    }
    //ok
    @PostMapping("/openProject")
    @ResponseBody //表示返回的是数据
    String openProject(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String projectName=request.getParameter("projectName");
            return sparkService.getProjectSchema(username,projectName).toString();
        }
        return null;
    }

    //ok
    @PostMapping("/createFolder")
    @ResponseBody //表示返回的是数据
    int createFolder(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String relativePath = request.getParameter("relativePath");
            return sparkService.createFolder(username,relativePath);
        }
        return 404;
    }
    //ok
    @PostMapping("/deleteFolder")
    @ResponseBody //表示返回的是数据
    boolean deleteFolder(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String relativePath = request.getParameter("relativePath");
            return sparkService.deleteFolder(username,relativePath);
        }
        return false;
    }
    //ok
    @PostMapping("/createFile")
    @ResponseBody //表示返回的是数据
    int createFile(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String relativePath = request.getParameter("relativePath");
            return sparkService.createFile(username,relativePath);
        }
        return 404;
    }
    //ok
    @PostMapping("/appendFile")
    @ResponseBody //表示返回的是数据
    int appendFile(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String relativePath = request.getParameter("relativePath");
            String fileText = request.getParameter("fileText");
            return sparkService.appendFile(username,relativePath,fileText);
        }
        return 404;
    }
    //ok
    @PostMapping("/createFileByCover")
    @ResponseBody //表示返回的是数据
    int createFileByCover(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String relativePath = request.getParameter("relativePath");
            String fileText = request.getParameter("fileText");
            return sparkService.createFileByCover(username,relativePath,fileText);
        }
        return 404;
    }
    //ok
    @PostMapping("/deleteFile")
    @ResponseBody //表示返回的是数据
    boolean deleteFile(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String relativePath = request.getParameter("relativePath");
            return sparkService.deleteFile(username,relativePath);
        }
        return false;
    }
    //ok
    @PostMapping("/getFileText")
    @ResponseBody //表示返回的是数据
    String getFileText(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String relativePath = request.getParameter("relativePath");
            return sparkService.getFileText(username,relativePath);
        }
        return null;
    }
    //ok
    @PostMapping("/renameFile")
    @ResponseBody //表示返回的是数据
    int renameFile(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            String username = user.getUserName();
            String relativePath = request.getParameter("relativePath");
            String newName = request.getParameter("newName");
            return sparkService.renameFile(username,relativePath,newName);
        }
        return 404;
    }
    //ok
    @PostMapping("/startCompute")
    @ResponseBody //表示返回的是数据
    int startCompute(HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        if(user==null) {
            return 404;
        }
        String username = user.getUserName();
        String email = user.getEmail();
        String projectName=request.getParameter("projectName");
        String runFilePath = request.getParameter("runFilePath");
        String cpu = request.getParameter("cpu");
        String memory = request.getParameter("memory");
        SparkUser sparkUser=sparkService.getSparkUser(username);
        if(sparkUser==null)
        {
            return 404;
        }
        sparkUser.setEmail(email);
        sparkUser.setProjectName(projectName);
        sparkUser.setRunFilePath(runFilePath);
        sparkUser.setCpu(cpu);
        sparkUser.setMemory(memory);
        sparkUser.setWebSocketServlet(pollWebSocketServlet);
        sparkUser.setMd5(md5Map.get(username+"/spark"));
        Thread thread=new Thread(sparkUser);
        thread.start();
        return 200;
    }
}
