package com.whut.controller;

import com.whut.domain.User;
import com.whut.service.OpenStackService;
import com.whut.service.SparkService;
import com.whut.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.TreeMap;


/**
 * 登录
 * 管理员admin 密码123 权限0(最高)
 *
 * @author Sandeepin
 */
@Controller
@RequestMapping("/authentication")
public class AuthenticationController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;
    @Autowired
    private OpenStackService openStackService;
    @Autowired
    private SparkService sparkService;

    private PasswordEncoder passwordEncoder;

    @Bean
    public PasswordEncoder passwordEncoder(){
        passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody //表示返回的是数据
    public int register(HttpServletRequest request) throws InterruptedException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String phone=request.getParameter("phone");
        String email=request.getParameter("email");
        if(userService.selectUserByUserName(username)!=null) {
            return 400;
        }
        if(userService.selectUserByPhone(phone)!=null){
            return 401;
        }
        if(userService.selectUserByEmail(email)!=null){
            return 402;
        }
        //设置用户的OpenStack配额为0,0,0,0即用户当前不可以创建虚拟机
        if(openStackService.registerOpenStackUser(username,password)){
            openStackService.setOpenStackUserQuota(username,"0","0","0","0","0",
                    "0","0","0");
            //数据库中添加一个用户
            User newUser=new User(username,password,"1",email,phone);
            userService.insertUser(newUser);
            request.getSession().setAttribute("user",newUser);
            //为用户创建一个Spark代码放置区
            sparkService.createSparkUser(username);
            return 200;
        }
        return 500;
    }
    // 登录
    @PostMapping(value = "/login")
    @ResponseBody
    public int login(HttpServletRequest request)
    {
        String usernameOrEmailOrPhone = request.getParameter("usernameOrEmailOrPhone");
        String password = request.getParameter("password");
        //根据用户名或者email或者phone查找用户
        User dataBaseUser = userService.selectUser(usernameOrEmailOrPhone);
        if(dataBaseUser==null) {
            return 403;//用户不存在！
        }else if(!dataBaseUser.getPassWord().equals(password)) {
            return 404;//密码错误！
        }else{
            request.getSession().setAttribute("user",dataBaseUser);
            logger.info(usernameOrEmailOrPhone+"登录！");
            return 200;
        }
    }
    // 获得session中的用户信息
    @PostMapping(value = "/getUserInfoInSession")
    @ResponseBody
    public Map<String, String> getUserInfoInSession(HttpServletRequest request)
    {
        Map<String, String> map=new TreeMap<>();
        String response="登陆";
        User user = (User)request.getSession().getAttribute("user");
        if(user!=null) {
            response = user.getUserName();
        }
        map.put("result",response);
        return map;
    }
}
