package com.whut.domain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by YY on 2017-12-27.
 */
public class HbaseTable {

    //表的属性
    private boolean isMeta=false;
    //表的属性集合
    private  Map<String,Object> tableProperty=null;
    //多列属性的集合，至少包含一列
    private  List<Map<String,Object>> columnSchema=null;

    public HbaseTable(HbaseColumn... hbaseColumns)
    {
        tableProperty=new TreeMap<>();
        columnSchema=new ArrayList<>();
        for(HbaseColumn hbaseColumn: hbaseColumns)
        {
            columnSchema.add(hbaseColumn.getHbaseColumnPoperty());
        }
    }
    //以下set函数是用来设置默认属性的函数
    public boolean isMeta() {
        return isMeta;
    }
    public void setMeta(boolean meta) {
        isMeta = meta;
        tableProperty.put("IS_META",this.isMeta);
    }
    //添加表的属性
    public void addTableProperty(String property,Object value)
    {
        tableProperty.put(property,value);
    }
    //重置原来列族的属性，并更改列族的名字
    public void addHbaseColum(HbaseColumn hbaseColumn)
    {
        columnSchema.add(hbaseColumn.getHbaseColumnPoperty());
    }
    //设置或添加完属性后，使用该函数获得Json对象
    public JSONObject getHbaseTableInJson()
    {
        JSONObject hbaseTableInJson=new JSONObject();
        try {
            //添加表的属性
            for(String key: tableProperty.keySet())
            {
                hbaseTableInJson.put(key,tableProperty.get(key));
            }
            //添加列的属性
            JSONArray jsonArray=new JSONArray();
            for(int i=0;i!=columnSchema.size();++i)
            {
                Map<String,Object> column = columnSchema.get(i);
                JSONObject object =new JSONObject();
                for(String key:column.keySet())
                {
                    object.put(key,column.get(key));
                }
                jsonArray.put(object);
            }
            hbaseTableInJson.put("ColumnSchema",jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return hbaseTableInJson;
    }
}

