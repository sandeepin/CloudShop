package com.whut.domain;

import javax.persistence.*;

/**
 * Created by YY on 2018-01-29.
 */
/**
 * 用户实体
 * 设置：用户名、密码、权限等级类型
 * @author Sandeepin
 */

@Entity
@Table(name = "openstack_user_quota")
public class OpenStackUserQuota {
    @Id
    @GeneratedValue
    private int id;
    @Column(name = "username")
    private String username;
    @Column(name = "maxinstance")
    private String maxinstance;
    @Column(name = "maxcpu")
    private String maxcpu;
    @Column(name = "maxram")
    private String maxram;
    @Column(name = "maxdisk")
    private String maxdisk;
    @Column(name = "currentinstance")
    private String currentinstance;
    @Column(name = "currentcpu")
    private String currentcpu;
    @Column(name = "currentram")
    private String currentram;
    @Column(name = "currentdisk")
    private String currentdisk;

    public OpenStackUserQuota(){

    }
    public OpenStackUserQuota(String username,String maxinstance,String maxcpu,String maxram,String maxdisk){
        this.username=username;
        this.maxinstance=maxinstance;
        this.maxcpu=maxcpu;
        this.maxram=maxram;
        this.maxdisk=maxdisk;
    }
    public OpenStackUserQuota(String username,String maxinstance,String maxcpu,String maxram,String maxdisk,
                       String currentinstance,String currentcpu,String currentram,String currentdisk){
        this.username=username;
        this.maxinstance=maxinstance;
        this.maxcpu=maxcpu;
        this.maxram=maxram;
        this.maxdisk=maxdisk;
        this.currentinstance=currentinstance;
        this.currentcpu=currentcpu;
        this.currentram=currentram;
        this.currentdisk=currentdisk;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMaxinstance() {
        return maxinstance;
    }

    public void setMaxinstance(String maxinstance) {
        this.maxinstance = maxinstance;
    }

    public String getMaxcpu() {
        return maxcpu;
    }

    public void setMaxcpu(String maxcpu) {
        this.maxcpu = maxcpu;
    }

    public String getMaxram() {
        return maxram;
    }

    public void setMaxram(String maxram) {
        this.maxram = maxram;
    }

    public String getMaxdisk() {
        return maxdisk;
    }

    public void setMaxdisk(String maxdisk) {
        this.maxdisk = maxdisk;
    }

    public String getCurrentinstance() {
        return currentinstance;
    }

    public void setCurrentinstance(String currentinstance) {
        this.currentinstance = currentinstance;
    }

    public String getCurrentcpu() {
        return currentcpu;
    }

    public void setCurrentcpu(String currentcpu) {
        this.currentcpu = currentcpu;
    }

    public String getCurrentram() {
        return currentram;
    }

    public void setCurrentram(String currentram) {
        this.currentram = currentram;
    }

    public String getCurrentdisk() {
        return currentdisk;
    }

    public void setCurrentdisk(String currentdisk) {
        this.currentdisk = currentdisk;
    }
}
