package com.whut.domain;

import com.whut.wesocket.PollWebSocketServlet;

/**
 * Created by YY on 2018-01-30.
 */
public class CreateParameter {
    private String username,password,virtualMachineName,image,cpu,ram,disk;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVirtualMachineName() {
        return virtualMachineName;
    }

    public void setVirtualMachineName(String virtualMachineName) {
        this.virtualMachineName = virtualMachineName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getDisk() {
        return disk;
    }

    public void setDisk(String disk) {
        this.disk = disk;
    }

    public CreateParameter(String username, String password, String virtualMachineName,
                           String image, String cpu, String ram, String disk){
        this.username=username;
        this.password=password;
        this.virtualMachineName=virtualMachineName;
        this.image=image;
        this.cpu=cpu;
        this.ram=ram;
        this.disk=disk;
    }
}
