package com.whut.domain;


import javax.persistence.*;

/**
 * Created by WSJ on 2017/11/2.
 */
@Entity
@Table(name = "openstack_vhost")
public class Vhost {
    //important 列名不能用'_'和大写字母
    @Id
    @GeneratedValue
    private int id;
    @Column(name = "username")
    private String username;
    @Column(name = "hostname")
    private String hostname;
    @Column(name = "hostid")
    private String hostid;
    @Column(name = "imagename")
    private String imagename;
    @Column(name = "cpu")
    private String cpu;
    @Column(name = "ram")
    private String ram;
    @Column(name = "disk")
    private String disk;
    @Column(name = "ip")
    private String ip;
    @Column(name = "url")
    private String url;
    @Column(name = "createtime")
    private String createtime;
    @Column(name = "islocked")
    private String islocked;
    @Column(name = "status")
    private String status;

    public String getIslocked() {
        return islocked;
    }

    public void setIslocked(String islocked) {
        this.islocked = islocked;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Vhost() {
    }
    //创建成功后可以知道的东西
    public Vhost(String username, String hostname,String imagename,String cpu,String ram,String disk,String createtime,String islocked) {
        this.username=username;
        this.hostname=hostname;
        this.imagename=imagename;
        this.cpu=cpu;
        this.ram=ram;
        this.disk=disk;
        this.createtime=createtime;
        this.islocked=islocked;
    }
    //得到创建信息后可以知道 ip、url、hostid、status
    public Vhost(String username, String hostname, String hostid, String imagename, String cpu, String ram, String disk, String ip, String url
            , String createtime, String islocked, String status) {
        this.username=username;
        this.hostname=hostname;
        this.hostid=hostid;
        this.imagename=imagename;
        this.cpu=cpu;
        this.ram=ram;
        this.disk=disk;
        this.ip=ip;
        this.url=url;
        this.createtime=createtime;
        this.islocked=islocked;
        this.status=status;
    }

    public String getHostid() {
        return hostid;
    }

    public void setHostid(String hostid) {
        this.hostid = hostid;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getImagename() {
        return imagename;
    }

    public void setImagename(String imagename) {
        this.imagename = imagename;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getDisk() {
        return disk;
    }

    public void setDisk(String disk) {
        this.disk = disk;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
}
