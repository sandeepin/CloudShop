package com.whut.domain;


import javax.persistence.*;

/**
 * 用户实体
 * 设置：用户名、密码、权限等级类型
 * @author Sandeepin
 */
@Entity
@Table(name = "openstack_user")
public class User {
    @Id
    @GeneratedValue
    private int id;
    @Column(name = "username")
    private String userName;
    @Column(name = "password")
    private String passWord;
    @Column(name = "level")
    private String levelType;
    @Column(name = "email")
    private String email;
    @Column(name = "phone")
    private String phone;

    public User() {
    }

    public User(String userName, String passWord, String levelType, String email, String phone) {
        this.userName = userName;
        this.passWord = passWord;
        this.levelType = levelType;
        this.email = email;
        this.phone = phone;
    }

    public User(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
        this.levelType = "1";
        this.email = "";
        this.phone = "";
    }

    public User(int id, String userName, String passWord, String levelType, String email, String phone) {
        this.id = id;
        this.userName = userName;
        this.passWord = passWord;
        this.levelType = levelType;
        this.email = email;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getLevelType() {
        return levelType;
    }

    public void setLevelType(String levelType) {
        this.levelType = levelType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}