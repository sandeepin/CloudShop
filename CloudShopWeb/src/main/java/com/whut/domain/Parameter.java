package com.whut.domain;

/**
 * Created by YY on 2018-01-30.
 */
public class Parameter {
    private String username;
    private String password;
    private String hostId;
    public Parameter(String username,String password,String hostId){
        this.username=username;
        this.password=password;
        this.hostId=hostId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }
}
