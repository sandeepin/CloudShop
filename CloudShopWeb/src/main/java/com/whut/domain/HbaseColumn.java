package com.whut.domain;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by YY on 2017-12-28.
 */
public class HbaseColumn {
    
    private Map<String,Object> columnPoperty=new TreeMap<>();
    //列的属性
    private String COLUMN_NAME =null;
    private String VERSIONS ="1";
    private Boolean EVICT_BLOCKS_ON_CLOSE=false;
    private Boolean NEW_VERSION_BEHAVIOR=false;
    private String KEEP_DELETED_CELLS ="FALSE";
    private String CACHE_DATA_ON_WRITE="FALSE";
    private String DATA_BLOCK_ENCODING="NONE"; //输入的值有待研究
    private Integer TTL=2147483647;//最大保存时间
    private String MIN_VERSIONS="0";
    private String REPLICATION_SCOPE="0";
    private String BLOOMFILTER="ROW";  //可以设置为"ROWCOL"
    private Boolean CACHE_INDEX_ON_WRITE=false;
    private Boolean IN_MEMORY=false;
    private Boolean CACHE_BLOOMS_ON_WRITE=false;
    private Boolean PREFETCH_BLOCKS_ON_OPEN=false;
    private String COMPRESSION="NONE";
    private Boolean CACHE_DATA_IN_L1=false;
    private Boolean BLOCKCACHE=true;
    private int BLOCKSIZE=65536;

    public HbaseColumn(String columnName)
    {
        columnPoperty.put("name",columnName);
    }
    public Map<String,Object> getHbaseColumnPoperty()
    {
        return columnPoperty;
    }
    //添加列族的属性
    public void addColumnProperty(String property,Object value)
    {
        columnPoperty.put(property,value);
    }
    public String getCOLUMN_NAME() {
        return COLUMN_NAME;
    }

    public void setCOLUMN_NAME(String COLUMN_NAME) {
        this.COLUMN_NAME = COLUMN_NAME;
        columnPoperty.put("name",this.COLUMN_NAME);
    }

    public String getVERSIONS() {
        return VERSIONS;
    }

    public void setVERSIONS(String VERSIONS) {
        this.VERSIONS = VERSIONS;
        columnPoperty.put("VERSIONS",this.VERSIONS);
    }

    public Boolean getEVICT_BLOCKS_ON_CLOSE() {
        return EVICT_BLOCKS_ON_CLOSE;
    }

    public void setEVICT_BLOCKS_ON_CLOSE(Boolean EVICT_BLOCKS_ON_CLOSE) {
        this.EVICT_BLOCKS_ON_CLOSE = EVICT_BLOCKS_ON_CLOSE;
        columnPoperty.put("EVICT_BLOCKS_ON_CLOSE",this.EVICT_BLOCKS_ON_CLOSE);
    }

    public Boolean getNEW_VERSION_BEHAVIOR() {
        return NEW_VERSION_BEHAVIOR;
    }

    public void setNEW_VERSION_BEHAVIOR(Boolean NEW_VERSION_BEHAVIOR) {
        this.NEW_VERSION_BEHAVIOR = NEW_VERSION_BEHAVIOR;
        columnPoperty.put("NEW_VERSION_BEHAVIOR",this.NEW_VERSION_BEHAVIOR);
    }

    public String getKEEP_DELETED_CELLS() {
        return KEEP_DELETED_CELLS;
    }

    public void setKEEP_DELETED_CELLS(String KEEP_DELETED_CELLS) {
        this.KEEP_DELETED_CELLS = KEEP_DELETED_CELLS;
        columnPoperty.put("KEEP_DELETED_CELLS",this.KEEP_DELETED_CELLS);
    }

    public String getCACHE_DATA_ON_WRITE() {
        return CACHE_DATA_ON_WRITE;
    }

    public void setCACHE_DATA_ON_WRITE(String CACHE_DATA_ON_WRITE) {
        this.CACHE_DATA_ON_WRITE = CACHE_DATA_ON_WRITE;
        columnPoperty.put("CACHE_DATA_ON_WRITE",this.CACHE_DATA_ON_WRITE);
    }

    public String getDATA_BLOCK_ENCODING() {
        return DATA_BLOCK_ENCODING;
    }

    public void setDATA_BLOCK_ENCODING(String DATA_BLOCK_ENCODING) {
        this.DATA_BLOCK_ENCODING = DATA_BLOCK_ENCODING;
        columnPoperty.put("DATA_BLOCK_ENCODING",this.DATA_BLOCK_ENCODING);
    }

    public Integer getTTL() {
        return TTL;
    }

    public void setTTL(Integer TTL) {
        this.TTL = TTL;
        columnPoperty.put("TTL",this.TTL);
    }

    public String getMIN_VERSIONS() {
        return MIN_VERSIONS;
    }

    public void setMIN_VERSIONS(String MIN_VERSIONS) {
        this.MIN_VERSIONS = MIN_VERSIONS;
        columnPoperty.put("MIN_VERSIONS",MIN_VERSIONS);
    }

    public String getREPLICATION_SCOPE() {
        return REPLICATION_SCOPE;
    }

    public void setREPLICATION_SCOPE(String REPLICATION_SCOPE) {
        this.REPLICATION_SCOPE = REPLICATION_SCOPE;
        columnPoperty.put("REPLICATION_SCOPE",this.REPLICATION_SCOPE);
    }

    public String getBLOOMFILTER() {
        return BLOOMFILTER;
    }

    public void setBLOOMFILTER(String BLOOMFILTER) {
        this.BLOOMFILTER = BLOOMFILTER;
        columnPoperty.put("BLOOMFILTER",this.BLOOMFILTER);
    }

    public Boolean getCACHE_INDEX_ON_WRITE() {
        return CACHE_INDEX_ON_WRITE;
    }

    public void setCACHE_INDEX_ON_WRITE(Boolean CACHE_INDEX_ON_WRITE) {
        this.CACHE_INDEX_ON_WRITE = CACHE_INDEX_ON_WRITE;
        columnPoperty.put("CACHE_INDEX_ON_WRITE",this.CACHE_INDEX_ON_WRITE);
    }

    public Boolean getIN_MEMORY() {
        return IN_MEMORY;
    }

    public void setIN_MEMORY(Boolean IN_MEMORY) {
        this.IN_MEMORY = IN_MEMORY;
        columnPoperty.put("IN_MEMORY",this.IN_MEMORY);
    }

    public Boolean getCACHE_BLOOMS_ON_WRITE() {
        return CACHE_BLOOMS_ON_WRITE;
    }

    public void setCACHE_BLOOMS_ON_WRITE(Boolean CACHE_BLOOMS_ON_WRITE) {
        this.CACHE_BLOOMS_ON_WRITE = CACHE_BLOOMS_ON_WRITE;
        columnPoperty.put("CACHE_BLOOMS_ON_WRITE",this.CACHE_BLOOMS_ON_WRITE);
    }

    public Boolean getPREFETCH_BLOCKS_ON_OPEN() {
        return PREFETCH_BLOCKS_ON_OPEN;
    }

    public void setPREFETCH_BLOCKS_ON_OPEN(Boolean PREFETCH_BLOCKS_ON_OPEN) {
        this.PREFETCH_BLOCKS_ON_OPEN = PREFETCH_BLOCKS_ON_OPEN;
        columnPoperty.put("PREFETCH_BLOCKS_ON_OPEN",this.PREFETCH_BLOCKS_ON_OPEN);
    }

    public String getCOMPRESSION() {
        return COMPRESSION;
    }

    public void setCOMPRESSION(String COMPRESSION) {
        this.COMPRESSION = COMPRESSION;
        columnPoperty.put("COMPRESSION",this.COMPRESSION);
    }

    public Boolean getCACHE_DATA_IN_L1() {
        return CACHE_DATA_IN_L1;
    }

    public void setCACHE_DATA_IN_L1(Boolean CACHE_DATA_IN_L1) {
        this.CACHE_DATA_IN_L1 = CACHE_DATA_IN_L1;
        columnPoperty.put("CACHE_DATA_IN_L1",this.CACHE_DATA_IN_L1);
    }

    public Boolean getBLOCKCACHE() {
        return BLOCKCACHE;
    }

    public void setBLOCKCACHE(Boolean BLOCKCACHE) {
        this.BLOCKCACHE = BLOCKCACHE;
        columnPoperty.put("BLOCKCACHE",this.BLOCKCACHE);
    }

    public int getBLOCKSIZE() {
        return BLOCKSIZE;
    }

    public void setBLOCKSIZE(int BLOCKSIZE) {
        this.BLOCKSIZE = BLOCKSIZE;
        columnPoperty.put("BLOCKSIZE",this.BLOCKSIZE);
    }
}
