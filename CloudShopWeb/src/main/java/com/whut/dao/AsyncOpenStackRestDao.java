package com.whut.dao;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * Created by YY on 2018-01-28.
 */
public interface AsyncOpenStackRestDao {

    //用户层次
    Future<Boolean> registerOpenStackUser(String username, String password);
    Future<Boolean> removeOpenStackUser(String username,String password);
    //虚拟机层次
    Future<Boolean> createVirtualMachine(String username,String password,String virtualMachineName,String image,String cpu,String ram,String disk);

    //创建完成后会得到虚拟机ID，通过hostid唯一确定虚拟机
    //包括电源状态、虚拟机状态、Ip信息
    Future<Map<String,String>> getVirtualMachineStatus(String username,String password,String hostID);
    Future<Map<String,String>> getVirtualMachineIp(String username,String password,String hostID);
    Future<Map<String,String>> getVirtualMachineConsoleUrl(String username,String password,String hostID);
    Future<Map<String,String>> getVirtualMachineConsoleUrlAndIp(String username,String password,String hostID);

    Future<Boolean> deleteVirtualMachine(String username,String password,String hostID);
    Future<Boolean> startVirtualMachine(String username,String password,String hostID);
    Future<Boolean> stopVirtualMachine(String username,String password,String hostID);
    Future<Boolean> rebootVirtualMachine(String username,String password,String hostID);
    Future<Boolean> suspendVirtualMachine(String username,String password,String hostID);
    Future<Boolean> resumeVirtualMachine(String username,String password,String hostID);
    Future<Boolean> pauseVirtualMachine(String username,String password,String hostID);
    Future<Boolean> unpauseVirtualMachine(String username,String password,String hostID);
    Future<Boolean> lockVirtualMachine(String username,String password,String hostID);
    Future<Boolean> unlockVirtualMachine(String username,String password,String hostID);






}
