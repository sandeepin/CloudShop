package com.whut.dao;
import java.util.Map;

/**
 * Created by YY on 2018-01-28.
 */
public interface OpenStackRestDao {

    //用户层次
    boolean registerOpenStackUser(String username, String password);
    boolean removeOpenStackUser(String username, String password);
    //虚拟机层次
    boolean createVirtualMachine(String username, String password, String virtualMachineName, String image, String cpu, String ram, String disk);

    //创建完成后会得到虚拟机ID，通过hostid唯一确定虚拟机
    //包括电源状态、虚拟机状态、Ip信息
    Map<String,String> getVirtualMachineStatus(String username, String password, String hostId);
    Map<String,String> getVirtualMachineIp(String username, String password, String hostId);
    Map<String,String> getVirtualMachineConsoleUrl(String username, String password, String hostId);
    Map<String,String> getVirtualMachineConsoleUrlAndIp(String username, String password, String hostId);
    boolean deleteVirtualMachine(String username, String password, String hostId);
    boolean startVirtualMachine(String username, String password, String hostId);
    boolean stopVirtualMachine(String username, String password, String hostId);
    boolean rebootVirtualMachine(String username, String password, String hostId);
    boolean suspendVirtualMachine(String username, String password, String hostId);
    boolean resumeVirtualMachine(String username, String password, String hostId);
    boolean pauseVirtualMachine(String username, String password, String hostId);
    boolean unpauseVirtualMachine(String username, String password, String hostId);
    boolean lockVirtualMachine(String username, String password, String hostId);
    boolean unlockVirtualMachine(String username, String password, String hostId);






}
