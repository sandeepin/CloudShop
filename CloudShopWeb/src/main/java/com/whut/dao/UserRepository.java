package com.whut.dao;

import com.whut.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by YY on 2017-11-27.
 */
public interface UserRepository extends JpaRepository<User,Integer> {

    User findUserByUserName(String userName);
    void deleteUserByUserName(String userName);
    User findUserByPhone(String phone);
    User findUserByEmail(String email);

}
