package com.whut.dao;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.Map;

/**
 * Created by YY on 2017-12-29.
 */
public interface IHBaseRestDao {

    JSONArray getHTableArray();
    JSONObject getHTableRegions(String tableName);
    JSONObject getHTableSchema(String tableName);
    Map<Boolean,String> createHTableByJson(String tableName, JSONObject strJson);
    Map<Boolean,String> updateHTableByJson(String tableName,JSONObject strJson);
    Map<Boolean,String> deleteHTable(String tableName);
    Map<Boolean,String> findRow(String tableName,String rowkey);
    Map<Boolean,String> findRowsByprefixFilter(String tableName,String prefixFilter,int maxCount);
    Map<Boolean,String> findRowsBySuffixFilter(String tableName,String suffixFilter,int maxCount);
    Map<Boolean,String> findRowsByRangFilter(String tableName,String startRow,String endRow);
    Map<Boolean,String> findCell(String tableName,String rowkey,String columnFamily,String column);
    Map<Boolean,String> findMultilVersionCell(String tableName,String rowkey,String columnFamily,String column,int n);
    Map<Boolean,String> insertCell(String tableName,String rowkey,String columnfamily,String column,String value);
    Map<Boolean,String> insertRow(String tableName,String rowkey,JSONObject jsonObject);
    Map<Boolean, String> insertRows(String tableName, JSONObject jsonObject);
    Map<Boolean,Map<String, String>> findFixColumnsBetweenRow(String tableName, String startRow, String endRow,String columnFamily,String column);


    //兼容原版
    // 单条操作
    Boolean add(String tableName, final String rowKey, final String family, final String qualifier, final String value);
    String find(String tableName, String rowKey, final String family, final String qualifier);
    // 批量操作
    Boolean adds(String tableName, final String rowKey, final String family, final Map<String,String> quaAndValue);
    Boolean adds(String tableName, final Map<String,String> rowkeyAndValue, final String family, final String qualifier);
    Boolean finds(String tableName, String startRow, String stopRow, final Map<String, String> outputRowkeyAndValue);

}
