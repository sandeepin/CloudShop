package com.whut.dao;

import com.whut.domain.OpenStackUserQuota;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 2018-01-29.
 */
public interface QuotaRepository extends JpaRepository<OpenStackUserQuota,Integer> {

    OpenStackUserQuota findOpenStackUserQuotaByUsername(String username);
    OpenStackUserQuota findOpenStackUserQuotaById(int id);
    void deleteOpenStackUserQuotaByUsername(String username);
    void deleteOpenStackUserQuotaById(int id);

}
