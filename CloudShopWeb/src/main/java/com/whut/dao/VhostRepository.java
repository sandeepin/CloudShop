package com.whut.dao;

import com.whut.domain.Vhost;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

/**
 * Created by YY on 2017-11-27.
 */

public interface VhostRepository extends JpaRepository<Vhost,Integer> {

   List<Vhost> findAllByUsername(String userName);
   Vhost findVhostByUsernameAndHostname(String username,String hostName);
   Vhost findVhostByHostid(String hostId);
   Vhost findVhostById(int ID);
   Vhost findVhostByIp(String ip);

   void deleteVhostByHostid(String hostId);
   void deleteVhostByUsernameAndHostname(String username,String hostName);
   void deleteAllByUsername(String userName);

}
