package com.whut.dao.impl;

import com.whut.dao.OpenStackRestDao;
import com.whut.util.HttpRequestUtil;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by YY on 2018-01-28.
 */
@Component
public class OpenStackRestDaoImpl implements OpenStackRestDao{

    private static final Logger logger = Logger.getLogger(OpenStackRestDaoImpl.class);
    private static String m_url="http://59.69.101.206:6078/";

    @Override
    public boolean registerOpenStackUser(String username, String password) {
        String url = m_url+"users";
        Map<String,String> map = new TreeMap<>();
        map.put("username",username);
        map.put("password",password);
        Map<Integer,String> result = HttpRequestUtil.httpPutByJson(url,new JSONObject(map));
        if(result.containsKey(200)) {
            return true;
        }
        logger.info(username+"注册失败");
        return false;
    }

    @Override
    public boolean removeOpenStackUser(String username, String password) {
        logger.info("removeOpenStackUser");
        return false;
    }

    @Override
    public boolean createVirtualMachine(String username, String password, String virtualMachineName, String image, String cpu, String ram, String disk) {
        String url=m_url+username+"/stacks";
        Map<String,String> map = new TreeMap<>();
        map.put("username",username);
        map.put("password",password);
        map.put("instance",virtualMachineName);
        map.put("image",image);
        map.put("vcpu",cpu);
        map.put("ram",ram);
        map.put("disk",disk);
        return HttpRequestUtil.httpPostByJson(url,new JSONObject(map)).containsKey(200);
    }

    @Override
    public Map<String, String> getVirtualMachineStatus(String username, String password, String hostID) {
        String url=String.format(m_url+username+"/stacks_status/"+hostID+"?username=%s&password=%s",username,password);
        Map<Integer,String> result = HttpRequestUtil.httpGet(url);
        Map<String,String> map=new TreeMap<>();
        if(result.containsKey(200)) {
            String string = result.get(200).replaceAll(" ","");
            string=string.substring(string.indexOf("power_state|")+"power_state|".length());
            String power_state = string.substring(0,string.indexOf("|"));
            string=string.substring(string.indexOf("vm_state|")+"vm_state|".length());
            String vm_state = string.substring(0,string.indexOf("|"));
            string=string.substring(string.indexOf("addresses|")+"addresses|".length());
            String addresses=string.substring(0,string.indexOf("|"));
            string=string.substring(string.indexOf("id|")+"id|".length());
            String id = string.substring(0,string.indexOf("|"));
            string=string.substring(string.indexOf("status|")+"status|".length());
            String status = string.substring(0,string.indexOf("|"));
            map.put("power_state",power_state);
            map.put("vm_state",vm_state);
            map.put("instance_ip",addresses);
            map.put("instance_id",id);
            map.put("status",status);
            return map;
        }
        return null;
    }
    @Override
    public Map<String,String> getVirtualMachineIp(String username, String password, String hostID) {
        String url=String.format(m_url+username+"/stacks_ip/"+hostID+"?username=%s&password=%s",username,password);
        Map<Integer,String> result = HttpRequestUtil.httpGet(url);
        Map<String,String> map =new TreeMap<>();
        if(result.containsKey(200))
        {
            String json=result.get(200);
            JSONObject object= null;
            try {
                object = new JSONObject(json);
                map.put("instance_ip",object.getString("instance_ip"));
                return map;
            }catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Map<String,String> getVirtualMachineConsoleUrl(String username, String password, String hostID) {
        String url=String.format(m_url+username+"/stacks_url/"+hostID+"?username=%s&password=%s",username,password);
        Map<Integer,String> result = HttpRequestUtil.httpGet(url);
        Map<String,String> map =new TreeMap<>();
        if(result.containsKey(200))
        {
            String json=result.get(200);
            JSONObject object= null;
            try {
                object = new JSONObject(json);
                map.put("instance_url",object.getString("instance_url"));
                return map;
            }catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Map<String, String> getVirtualMachineConsoleUrlAndIp(String username, String password, String hostID) {
        String url=String.format(m_url+username+"/stacks/"+hostID+"?username=%s&password=%s",username,password);
        Map<Integer,String> result = HttpRequestUtil.httpGet(url);
        Map<String,String> map =new TreeMap<>();
        if(result.containsKey(200)) {
            String json=result.get(200);
            JSONObject object= null;
            try {
                object = new JSONObject(json);
                map.put("instance_ip",object.getString("instance_ip")) ;
                map.put("instance_url",object.getString("instance_url"));
            }catch (JSONException e) {
                e.printStackTrace();
            }
            return map;
        }
       return null;
    }

    private boolean operateVirtualMachine(String username,String password,String hostID,int options) {
        String url = m_url + username + "/stacks/" + hostID;
        Map<String, String> map = new TreeMap<>();
        map.put("username", username);
        map.put("password", password);
        map.put("options", String.valueOf(options));
        return HttpRequestUtil.httpDeleteByJson(url, new JSONObject(map)).containsKey(200);
    }

    @Override
    public boolean deleteVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,0);
    }

    @Override
    public boolean startVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,1);
    }

    @Override
    public boolean stopVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,2);
    }

    @Override
    public boolean rebootVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,3);
    }

    @Override
    public boolean suspendVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,4);
    }

    @Override
    public boolean resumeVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,5);
    }

    @Override
    public boolean pauseVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,6);
    }

    @Override
    public boolean unpauseVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,7);
    }

    @Override
    public boolean lockVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,8);
    }

    @Override
    public boolean unlockVirtualMachine(String username, String password, String hostID) {
        return operateVirtualMachine(username,password,hostID,9);
    }
}
