package com.whut;

import com.whut.service.SparkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
/**
 * @author Sandeepin
 */
@EnableAsync
@SpringBootApplication
public class CloudshopApplication implements CommandLineRunner {
    @Autowired
    SparkService sparkService;
    public static void main(String[] args) {
        SpringApplication.run(CloudshopApplication.class, args);
    }
    @Override
    public void run(String... strings) throws Exception {
        sparkService.initSparkService();
    }
}
//@SpringBootApplication
//@EnableCaching
//@EnableAsync
//public class CloudshopApplication extends SpringBootServletInitializer implements CommandLineRunner{
//    @Autowired
//    SparkService sparkService;
//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(CloudshopApplication.class);
//    }
//    public static void main(String[] args) {
//        SpringApplication.run(CloudshopApplication.class, args);
//    }
//    @Override
//    public void run(String... strings) throws Exception {
//        sparkService.initSparkService();
//    }
//}

