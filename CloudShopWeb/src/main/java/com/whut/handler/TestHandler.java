package com.whut.handler;

import com.jcraft.jsch.*;
import com.whut.util.JSchUtil;
import com.whut.util.SshUtil;
import com.whut.util.TimeFormatUtil;

import java.io.*;

/**
 * Created by YY on 2018-02-02.
 */
public class TestHandler {

    public static void main(String[] args){
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession("ubuntu", "59.69.101.206", 30422);
            session.setPassword("522578");
            UserInfo userInfo = new JSchUtil.MyUserInfo();
            session.setUserInfo(userInfo);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(30000);   // making a connection with timeout.
            Channel channel=session.openChannel("shell");
            //打开通道后，设置通道的输入输出流
            InputStream inputStream = channel.getInputStream();
            OutputStream outputStream =channel.getOutputStream();

            channel.connect(30000);
            byte[] buffer=new byte[1024];
            while(true){
                if(System.in.read()>0){
                    outputStream.write("ls\n".getBytes());
                    outputStream.flush();
                }
                while(inputStream.available()>0){
                    int i=inputStream.read(buffer, 0, 1024);
                    if(i<0)break;
                    System.out.print(new String(buffer, 0, i));
                }
                if(channel.isClosed()){
                    if(inputStream.available()>0) continue;
                    System.out.print("exit-status: "+channel.getExitStatus());
                    break;
                }
            }
        } catch (JSchException | IOException e) {
            e.printStackTrace();
        }
    }

}
