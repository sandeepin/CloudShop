package com.whut.handler;

import com.whut.domain.CreateParameter;
import com.whut.service.OpenStackService;
import com.whut.util.SendMessageUtil;
import com.whut.wesocket.PollWebSocketServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Map;

/**
 * Created by YY on 2018-01-30.
 */
@Service
public class CreateSigleVMHandler implements Runnable{
    private CreateParameter parameter;
    private PollWebSocketServlet webSocketServlet;
    private String md5;
    @Autowired
    private OpenStackService openStackService;

    public void setParameterOfHandler(CreateParameter parameter) {
        this.parameter = parameter;
    }
    public void setWebSocketServlet(String md5, PollWebSocketServlet webSocketServlet){
        this.md5=md5;
        this.webSocketServlet=webSocketServlet;
    }
    @Override
    public void run() {
        Map<String,String> map = openStackService.createVMAndAsyncGetInfo(parameter.getUsername(),parameter.getPassword(),
                parameter.getVirtualMachineName(),parameter.getImage(),parameter.getCpu()
                ,parameter.getRam(),parameter.getDisk());
        try {
            if(!map.isEmpty()) {
                SendMessageUtil.sendMessage(md5,map,webSocketServlet);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
