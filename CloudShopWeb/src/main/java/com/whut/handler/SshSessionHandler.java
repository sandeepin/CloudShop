package com.whut.handler;

import com.jcraft.jsch.*;
import com.whut.util.JSchUtil;
import com.whut.wesocket.SshWebSocketServlet;

import javax.swing.*;
import java.io.*;

/**
 * Created by YY on 2018-02-02.
 */
public class SshSessionHandler implements Runnable{

    private String key;
    private SshWebSocketServlet webSocketServlet;
    private String username;
    private String password;
    private String address;
    private String userInput="";
    private boolean run=true;

    public void setUserInput(String userInput){
        this.userInput=userInput;
    }
    public void stopHandler(){
        run=false;
    }
    public void setWebSocketServlet(SshWebSocketServlet webSocketServlet, String key) {
        this.key = key;
        int index = key.indexOf(":");
        username = key.substring(0, index);
        key = key.substring(index + 1);
        index = key.indexOf("@");
        password = key.substring(0, index);
        address = key.substring(index + 1);
        this.webSocketServlet = webSocketServlet;
    }
    @Override
    public void run() {
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(username, address, 22);
            session.setPassword(password);
            UserInfo userInfo = new JSchUtil.MyUserInfo();
            session.setUserInfo(userInfo);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(30000);   // making a connection with timeout.
            Channel channel=session.openChannel("shell");
            InputStream inputStream = channel.getInputStream();
            OutputStream outputStream = channel.getOutputStream();
            channel.connect(3*1000);
            byte[] buffer=new byte[2048];
            while(run){
                if(userInput.length()>0){
                    outputStream.write((userInput).getBytes());
                    outputStream.flush();
                    userInput="";
                }
                while(inputStream.available()>0){
                    int i=inputStream.read(buffer, 0, 2048);
                    if(i<0)break;
                    webSocketServlet.sendMes(key,new String(buffer, 0, i));
                }
                if(channel.isClosed()){
                    if(inputStream.available()>0) continue;
                    break;
                }
            }
//            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
//            String msg = null;
//            while (run){
//                if(userInput.length()>0){
//                    outputStream.write((userInput).getBytes());
//                    outputStream.flush();
//                    userInput="";
//                }
//                while(inputStream.available()>0){
//                    webSocketServlet.sendMes(key,in.readLine()+"\r\n");
//                }
//                if(channel.isClosed()){
//                    if(inputStream.available()>0) continue;
//                    break;
//                }
//            }
        } catch (JSchException | IOException e) {
            e.printStackTrace();
        }
    }
}
