package com.whut.handler.extend;

import com.whut.handler.AsyncMultiTaskHandler;
import com.whut.service.AsyncOpenStackService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * Created by YY on 2018-01-30.
 */
@Service
public class AsyncUnlockMultiVMHandler extends AsyncMultiTaskHandler{

    public AsyncUnlockMultiVMHandler(AsyncOpenStackService asyncOpenStackService) {
        super(asyncOpenStackService);
    }

    @Override
    public void run() {
        List<Future<Map<String, String>>> taskList=new ArrayList<>();
        for(int i=0;i!=parameters.length;++i){
            taskList.add(asyncOpenStackService.unlockVirtualMachine(parameters[i].getUsername(),parameters[i].getPassword(),
                    parameters[i].getHostId()));
        }
       waitTaskDone(taskList);
    }
}
