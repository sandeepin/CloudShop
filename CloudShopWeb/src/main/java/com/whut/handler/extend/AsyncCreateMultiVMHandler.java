package com.whut.handler.extend;

import com.whut.handler.AsyncMultiTaskHandler;
import com.whut.service.AsyncOpenStackService;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;


/**
 * Created by YY on 2018-01-30.
 */
@Service
public class AsyncCreateMultiVMHandler extends AsyncMultiTaskHandler{

    public AsyncCreateMultiVMHandler(AsyncOpenStackService asyncOpenStackService) {
        super(asyncOpenStackService);
    }

    @Override
    public void run() {
        List<Future<Map<String,String>>> taskList=new ArrayList<>();
        for(int i=0;i!=createParameters.length;++i){
            taskList.add(asyncOpenStackService.createVMAndAsyncGetInfo(createParameters[i].getUsername(),createParameters[i].getPassword(),
                    createParameters[i].getVirtualMachineName(),createParameters[i].getImage(),createParameters[i].getCpu()
                    ,createParameters[i].getRam(),createParameters[i].getDisk()));
        }
        waitTaskDone(taskList);
    }
}
