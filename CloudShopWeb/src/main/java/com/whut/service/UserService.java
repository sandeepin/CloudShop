package com.whut.service;

import com.whut.domain.User;



/**
 * @author YY
 */
public interface UserService {

    void insertUser(User user);
    void deleteUser(String userName);
    void updateUser(String userName,User user);
    User selectUser(String usernameOrEmailOrPhone);
    User selectUserByUserName(String username);
    User selectUserByPhone(String phone);
    User selectUserByEmail(String email);

}
