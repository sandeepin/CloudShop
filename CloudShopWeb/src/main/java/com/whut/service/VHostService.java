package com.whut.service;

import com.whut.domain.Vhost;

import java.util.List;
import java.util.Map;

/**
 * Created by YY on 2017-11-18.
 */
public interface VHostService {

    void insertVhost(Vhost vhost);

    Vhost updateVhost(String hostId,Vhost vhost);
    Vhost updateVhostIp(String hostId,String instance_ip);
    Vhost updateVhostUrl(String hostId,String instance_url);
    Vhost updateVhostIsLocked(String hostId,String isLocked);
    Vhost updateVhostStatus(String hostId,String status);

    Vhost updateVhost(int ID,Vhost vhost);
    Vhost updateVhostIp(int ID,String instance_ip);
    Vhost updateVhostUrl(int ID,String instance_url);
    Vhost updateVhostIsLocked(int ID,String isLocked);
    Vhost updateVhostStatus(int ID,String status);

    void deleteVhostsByUserName(String userName);
    void deleteVhostByHostId(String hostId);

    Vhost selectVhostByID(int ID);
    Map<String,String> selectVhostByIDToMap(int ID);
    Vhost selectVhostByHostId(String hostId);
    Map<String,String> selectVhostByHostIdToMap(String hostId);
    List<Vhost> selectVhostsByUserName(String userName);
    List<Map<String,String>> selectVhostsByUserNameToListMap(String userName);

}
