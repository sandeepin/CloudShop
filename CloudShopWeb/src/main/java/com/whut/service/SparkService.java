package com.whut.service;

import com.whut.domain.SparkUser;
import org.json.JSONObject;
/**
 * Created by YY on 2018-01-21.
 */
public interface SparkService {
    void initSparkService();
    int createSparkUser(String username);
    SparkUser getSparkUser(String username);
    boolean deleteSparkUser(String username);
    int createProject(String username,String projectName);
    boolean deleteProject(String username, String projectName);
    JSONObject getProjectSchema(String username, String projectName);
    JSONObject getAllProjectSchema(String username);
    int createFolder(String username,String relativePath);
    boolean deleteFolder(String username, String relativePath);
    int createFile(String username,String relativePath);
    int appendFile(String username,String relativePath,String text);
    int createFileByCover(String username,String relativePath,String text);
    boolean deleteFile(String username, String relativePath);
    String getFileText(String username,String relativePath);
    int renameFile(String username,String relativePath,String newName);
}
