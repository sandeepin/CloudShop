package com.whut.service;

import java.util.Map;
import java.util.concurrent.Future;

/**
 * Created by YY on 2018-01-30.
 */
public interface AsyncOpenStackService {

    //虚拟机层次
    Future<Boolean> createVirtualMachine(String username,String password,String virtualMachineName,String image,String cpu,String ram,String disk);
    Future<Map<String,String>> createVMAndAsyncGetInfo(String username,String password,String virtualMachineName,
                                               String image,String cpu,String ram,String disk);
    //包括电源状态、虚拟机状态、Ip信息
    Future<Map<String,String>> getStatusAndUpdateDatabase(String username,String password,String hostId);
    Future<Map<String,String>> getUrlAndUpdateDatabase(String username,String password,String hostId);
    //争对单台虚拟机的异步操作，返回虚拟机ID和操作结果
    Future<Map<String,String>> deleteVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> startVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> stopVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> rebootVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> suspendVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> resumeVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> pauseVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> unpauseVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> lockVirtualMachine(String username,String password,String hostId);
    Future<Map<String,String>> unlockVirtualMachine(String username,String password,String hostId);

}
