package com.whut.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * Created by YY on 2018-01-29.
 */
public interface OpenStackService {

    //对用户的配额进行管理
    void setOpenStackUserQuota(String username,String maxInstance, String maxCpu, String maxRam, String maxDisk,
                               String currentInstance,String currentCpu,String currentRam,String currentDisk);
    boolean updateOpenStackUserCurrentQuota(String username,boolean isAdd,String instance,String cpu,String ram,String disk);
    boolean updateOpenStackUserMaxQuota(String username,boolean isAdd,String instance,String cpu,String ram,String disk);
    Map<String,String> getLeftoverQuota(String username);
    boolean checkCanCreate(String username,String useInstance,String useCpu,String useRam,String useDisk);
    //对虚拟机生命周期和当前能进行的操作进行管理
    List<String> getVirtualMachineCurrentAvailableOptions(String hostID);
    List<Integer> getVirtualMachineCurrentAvailableOptionsCode(String hostID);
    //利用OpenstackRestDao实现各种功能，并更新数据库
    //用户层次
    boolean registerOpenStackUser(String username,String password);
    boolean removeOpenStackUser(String username,String password);
    //虚拟机层次
    boolean createVirtualMachine(String username,String password,String virtualMachineName,String image,String cpu,String ram,String disk);
    Map<String,String> createVMAndAsyncGetInfo(String username,String password,String virtualMachineName,
                                                 String image,String cpu,String ram,String disk);
    //争对单台虚拟机的操作，返回HostID和操作结果
    //包括电源状态、虚拟机状态、Ip信息
    Map<String,String> getStatusAndUpdateDatabase(String username,String password,String hostID);
    Map<String,String> getUrlAndUpdateDatabase(String username,String password,String hostID);
    Map<String,String> deleteVirtualMachine(String username,String password,String hostID);
    Map<String,String> startVirtualMachine(String username,String password,String hostID);
    Map<String,String> stopVirtualMachine(String username,String password,String hostID);
    Map<String,String> rebootVirtualMachine(String username,String password,String hostID);
    Map<String,String> suspendVirtualMachine(String username,String password,String hostID);
    Map<String,String> resumeVirtualMachine(String username,String password,String hostID);
    Map<String,String> pauseVirtualMachine(String username,String password,String hostID);
    Map<String,String> unpauseVirtualMachine(String username,String password,String hostID);
    Map<String,String> lockVirtualMachine(String username,String password,String hostID);
    Map<String,String> unlockVirtualMachine(String username,String password,String hostID);
}

