package com.whut.service;

import com.whut.domain.OpenStackUserQuota;

import java.util.Map;

/**
 * Created by YY on 2018-01-29.
 */
public interface QuotaService {

    void insertQuota(OpenStackUserQuota quota);
    void deleteQuotaByUserName(String userName);
    void deleteQuotaById(int id);
    void updateQuota(String username,OpenStackUserQuota quota);

    OpenStackUserQuota selectQuotaByUserName(String userName);
    OpenStackUserQuota selectQuotaById(int id);
}
