package com.whut.service.impl;

import com.whut.dao.QuotaRepository;
import com.whut.domain.OpenStackUserQuota;
import com.whut.service.QuotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by YY on 2018-01-29.
 */
@Service
public class QuotaServiceImpl implements QuotaService {

    private final QuotaRepository quotaRepository;

    @Autowired
    public QuotaServiceImpl(QuotaRepository quotaRepository) {
        this.quotaRepository = quotaRepository;
    }

    @Override
    public void insertQuota(OpenStackUserQuota quota) {
        quotaRepository.save(quota);
    }

    @Override
    public void deleteQuotaByUserName(String userName) {
        quotaRepository.deleteOpenStackUserQuotaByUsername(userName);
    }

    @Override
    public void deleteQuotaById(int id) {
        quotaRepository.deleteOpenStackUserQuotaById(id);
    }

    @Override
    public void updateQuota(String username, OpenStackUserQuota quota) {
        OpenStackUserQuota quota1 = quotaRepository.findOpenStackUserQuotaByUsername(username);
        if(quota1!=null){
            quota.setId(quota1.getId());
            quotaRepository.save(quota);
        }
    }

    @Override
    public OpenStackUserQuota selectQuotaByUserName(String userName) {
         return quotaRepository.findOpenStackUserQuotaByUsername(userName);
    }

    @Override
    public OpenStackUserQuota selectQuotaById(int id) {
        return quotaRepository.findOpenStackUserQuotaById(id);
    }

}
