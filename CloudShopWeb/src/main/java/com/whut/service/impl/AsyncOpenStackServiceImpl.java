package com.whut.service.impl;

import com.whut.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.Future;

/**
 * Created by YY on 2018-01-30.
 */
@Service
public class AsyncOpenStackServiceImpl implements AsyncOpenStackService {

    @Autowired
    private OpenStackService openStackService;
    @Async
    @Override
    public Future<Boolean> createVirtualMachine(String username, String password, String virtualMachineName,
                                        String image, String cpu, String ram, String disk) {
       return new AsyncResult<>(openStackService.createVirtualMachine(username,password,virtualMachineName,
               image, cpu, ram, disk));
    }
    @Async
    @Override
    public Future<Map<String, String>> createVMAndAsyncGetInfo(String username, String password, String virtualMachineName, String image, String cpu, String ram, String disk) {
        return new AsyncResult<>(openStackService.createVMAndAsyncGetInfo(username,password,virtualMachineName,image,cpu,ram,disk));
    }
    @Async
    @Override
    public Future<Map<String, String>> deleteVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.deleteVirtualMachine(username,password,virtualMachineName));
    }
    @Async
    @Override
    public  Future<Map<String, String>> getStatusAndUpdateDatabase(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.getStatusAndUpdateDatabase(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String,String>> getUrlAndUpdateDatabase(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.getUrlAndUpdateDatabase(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String, String>> startVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.startVirtualMachine(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String, String>> stopVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.stopVirtualMachine(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String, String>> rebootVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.rebootVirtualMachine(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String, String>> suspendVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.suspendVirtualMachine(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String, String>> resumeVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.resumeVirtualMachine(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String, String>> pauseVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.pauseVirtualMachine(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String, String>> unpauseVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.unpauseVirtualMachine(username,password,virtualMachineName));
    }
    @Async
    @Override
    public Future<Map<String, String>> lockVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.lockVirtualMachine(username,password,virtualMachineName));
    }

    @Async
    @Override
    public Future<Map<String, String>> unlockVirtualMachine(String username, String password, String virtualMachineName) {
        return new AsyncResult<>(openStackService.unlockVirtualMachine(username,password,virtualMachineName));
    }
}
