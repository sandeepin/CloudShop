package com.whut.service.impl;

import com.whut.dao.UserRepository;
import com.whut.domain.User;
import com.whut.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Sandeepin
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void insertUser(User user) {

        userRepository.save(user);

    }
    @Override
    public void deleteUser(String userName) {
        userRepository.deleteUserByUserName(userName);
    }

    @Override
    public void updateUser(String userName,User user) {

        User user1=userRepository.findUserByUserName(userName);
        if(user1!=null) {
            user.setId(user1.getId());
            userRepository.save(user);
        }
    }

    @Override
    public User selectUser(String usernameOrEmailOrPhone) {
        User user=null;
       if((user=userRepository.findUserByUserName(usernameOrEmailOrPhone))!=null) {
           return user;
       }else if((user=userRepository.findUserByPhone(usernameOrEmailOrPhone))!=null){
            return user;
        }else {
           return userRepository.findUserByEmail(usernameOrEmailOrPhone);
       }
    }

    @Override
    public User selectUserByUserName(String username) {
        return userRepository.findUserByUserName(username);
    }

    @Override
    public User selectUserByPhone(String phone) {
        return userRepository.findUserByPhone(phone);
    }

    @Override
    public User selectUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }
}