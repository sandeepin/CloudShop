package com.whut.service.impl;

import com.whut.dao.UserRepository;
import com.whut.domain.SparkUser;
import com.whut.service.SparkService;
import com.whut.util.FileUtil;
import com.whut.util.OSUtil;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by YY on 2018-01-21.
 */
@Service
public class SparkServiceImpl implements SparkService{

    private static final Logger logger = Logger.getLogger(SparkServiceImpl.class);
    private static Map<String,SparkUser> userMap=new TreeMap<>();
    private String basePath;

    @Override
    public void initSparkService()
    {
        if(OSUtil.isWindow())
            basePath="C:\\Users\\Administrator\\Desktop";
        else
            basePath="/home/ubuntu/spark";
        String[] usernameList = FileUtil.getDirectoryList(basePath);
        if(usernameList==null){
            return;
        }
        for(int i=0;i!=usernameList.length;++i){
            userMap.put(usernameList[i],new SparkUser(usernameList[i]));
        }
    }
    @Override
    public int createSparkUser(String username)
    {
        int code = FileUtil.newFolder(basePath+"/"+username);
        if(code==200){
            userMap.put(username,new SparkUser(username));
        }
        return code;
    }
    @Override
    public SparkUser getSparkUser(String username) {
        return userMap.get(username);
    }
    @Override
    public boolean deleteSparkUser(String username)
    {
        boolean result=true;
        SparkUser sparkUser=userMap.get(username);
        if(sparkUser!=null) {
            result&=FileUtil.delFolder(basePath+"/"+username);
            if(result)
                userMap.remove(username);
        }
        return result;
    }
    @Override
    public int createProject(String username, String projectName) {
        int code=400;
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null) {
            code = sparkUser.createProject(projectName);
        }
        return code;
    }
    @Override
    public boolean deleteProject(String username, String projectName) {
        boolean b=true;
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null) {
            b&= sparkUser.deleteProject(projectName);
        }
        return b;
    }
    @Override
    public JSONObject getProjectSchema(String username, String projectName)
    {
        JSONObject jsonObject=new JSONObject();
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            jsonObject = sparkUser.getProjectSchema(projectName);
        }
        return jsonObject;
    }
    @Override
    public JSONObject getAllProjectSchema(String username) {
        JSONObject jsonObject=new JSONObject();
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            jsonObject = sparkUser.getAllProjectSchema();
        }

        return jsonObject;
    }

    @Override
    public int createFolder(String username, String relativePath)
    {
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            return sparkUser.createFolder(relativePath);
        }
        return 400;//用户名或者项目不存在
    }
    @Override
    public boolean deleteFolder(String username, String relativePath)
    {
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            return sparkUser.deleteFolder(relativePath);
        }
        return false;
    }
    @Override
    public int createFile(String username, String relativePath)
    {
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            return sparkUser.createFile(relativePath,"");
        }
        return 400;
    }
    @Override
    public int appendFile(String username,String relativePath, String text)
    {
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            return sparkUser.appendFile(relativePath,text);
        }
        return 400;
    }
    @Override
    public int createFileByCover(String username,String relativePath, String text)
    {
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null) {
            return sparkUser.createFileByCover(relativePath,text);
        }
        return 400;
    }
    @Override
    public boolean deleteFile(String username,String relativePath)
    {
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            return sparkUser.deleteFile(relativePath);
        }
        return false;
    }
    @Override
    public String getFileText(String username, String relativePath) {
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            return sparkUser.getFileText(relativePath);
        }
        return null;
    }
    @Override
    public int renameFile(String username,String relativePath,String newName)
    {
        SparkUser sparkUser = userMap.get(username);
        if(sparkUser!=null)
        {
            return sparkUser.renameFile(relativePath,newName);
        }
        return 404;
    }
}
