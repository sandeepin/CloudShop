package com.whut.service.impl;

import com.whut.dao.VhostRepository;
import com.whut.domain.Vhost;
import com.whut.service.VHostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by YY on 2017-11-18.
 */
@Service
public class VHostServiceImpl implements VHostService{


    @Autowired
    VhostRepository vhostRepository;

    @Override
    public void insertVhost(Vhost vhost) {
        vhostRepository.save(vhost);
    }

    @Override
    public void deleteVhostsByUserName(String userName) {
        vhostRepository.deleteAllByUsername(userName);
    }
    @Override
    @Transactional
    public void deleteVhostByHostId(String hostId) {
        vhostRepository.deleteVhostByHostid(hostId);
    }

    @Override
    public Vhost selectVhostByID(int ID) {
        return vhostRepository.findVhostById(ID);
    }

    @Override
    public Map<String, String> selectVhostByIDToMap(int ID) {
        return vhost2Map(vhostRepository.findVhostById(ID));
    }

    //返回更新后的Vhost，若更新失败，则Vhost不变
    @Override
    public Vhost updateVhost(String hostId, Vhost vhost) {
       Vhost vhost1=vhostRepository.findVhostByHostid(hostId);
       if(vhost1!=null) {
           vhost.setId(vhost1.getId());
           vhostRepository.save(vhost);
       }
       return vhost;
    }

    @Override
    public Vhost updateVhostIp(String hostId, String instance_ip) {
        Vhost vhost=vhostRepository.findVhostByHostid(hostId);
        if(vhost!=null) {
            vhost.setIp(instance_ip);
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    public Vhost updateVhostUrl(String hostId, String instance_url) {

        Vhost vhost=vhostRepository.findVhostByHostid(hostId);
        if(vhost!=null)
        {
            vhost.setUrl(instance_url);
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    public Vhost updateVhostIsLocked(String hostId, String isLocked) {
        Vhost vhost=vhostRepository.findVhostByHostid(hostId);
        if(vhost!=null) {
            vhost.setIslocked(isLocked);
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    public Vhost updateVhostStatus(String hostId, String status) {
        Vhost vhost=vhostRepository.findVhostByHostid(hostId);
        if(vhost!=null)
        {
            vhost.setStatus(status);
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    public Vhost updateVhost(int ID, Vhost vhost) {
        Vhost vhost1=vhostRepository.findVhostById(ID);
        if(vhost1!=null) {
            vhost.setId(vhost1.getId());
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    public Vhost updateVhostIp(int ID, String instance_ip) {
        Vhost vhost=vhostRepository.findVhostById(ID);
        if(vhost!=null) {
            vhost.setIp(instance_ip);
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    public Vhost updateVhostUrl(int ID, String instance_url) {
        Vhost vhost=vhostRepository.findVhostById(ID);
        if(vhost!=null) {
            vhost.setUrl(instance_url);
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    public Vhost updateVhostIsLocked(int ID, String isLocked) {
        Vhost vhost=vhostRepository.findVhostById(ID);
        if(vhost!=null) {
            vhost.setIslocked(isLocked);
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    public Vhost updateVhostStatus(int ID, String status) {
        Vhost vhost=vhostRepository.findVhostById(ID);
        if(vhost!=null) {
            vhost.setStatus(status);
            vhostRepository.save(vhost);
        }
        return vhost;
    }

    @Override
    @Transactional
    public Vhost selectVhostByHostId(String hostId) {
        return vhostRepository.findVhostByHostid(hostId);
    }

    @Override
    public Map<String, String> selectVhostByHostIdToMap(String hostId) {
        return vhost2Map(selectVhostByHostId(hostId));
    }

    @Override
    public List<Vhost> selectVhostsByUserName(String userName) {
        return vhostRepository.findAllByUsername(userName);
    }

    @Override
    public List<Map<String, String>> selectVhostsByUserNameToListMap(String userName) {
        List<Map<String, String>> mapList =new ArrayList<>();
        List<Vhost> list = selectVhostsByUserName(userName);
        if(list!=null){
            for(int i=0;i!=list.size();++i){
                mapList.add(vhost2Map(list.get(i)));
            }
        }
        return mapList;
    }

    private static Map<String,String> vhost2Map(Vhost vhost)
    {
        Map<String,String> map =new TreeMap<>();
        if(vhost!=null){
            map.put("instance",vhost.getHostname());
            map.put("username",vhost.getUsername());
            map.put("image",vhost.getImagename());
            map.put("vcpu",vhost.getCpu());
            map.put("ram",vhost.getRam());
            map.put("disk",vhost.getDisk());
            map.put("instance_time",vhost.getCreatetime());
            map.put("instance_ip",vhost.getIp());
            map.put("instance_url",vhost.getUrl());
            map.put("instance_id",vhost.getHostid());
            map.put("ID",""+vhost.getId());
            map.put("status",vhost.getStatus());
            map.put("islocked",vhost.getIslocked());
        }
        return map;
    }
}
