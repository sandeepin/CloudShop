package com.whut.netty;

import com.whut.netty.handler.NettyServerHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.nio.charset.Charset;


/**
 * Created by YY on 2017-11-23.
 */
public class NettyServerFilter extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline ph =ch.pipeline();
        // 根据自定义分隔符组合成完整的包
        ByteBuf delimiter = Unpooled.copiedBuffer("!".getBytes());
        //增大缓冲区间大小
        ph.addLast("package", new DelimiterBasedFrameDecoder(20*1024, delimiter));
        // 设置编解码器
        ph.addLast("encoder", new StringEncoder(Charset.forName("UTF-8")));
        ph.addLast("decoder", new StringDecoder(Charset.forName("GBK")));
        ph.addLast("hanlder1", new NettyServerHandler());
    }
}
