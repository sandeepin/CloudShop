package com.whut.netty.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.util.Date;

/**
 * Created by YY on 2017-11-18.
 */
@Service("NettyServerHandler")
@ChannelHandler.Sharable
public class NettyServerHandler extends SimpleChannelInboundHandler<String>
{
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String msg) throws Exception
    {
        // 收到消息直接打印输出
        System.out.println("服务端接受的消息 : " + msg);
        if("quit".equals(msg)) {//服务端断开的条件
            channelHandlerContext.close();
        }
        Date date=new Date();
        // 返回客户端消息
        channelHandlerContext.writeAndFlush(date+"\n");
    }
    /*
   * 建立连接时，返回消息
   */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception
    {
        System.out.println("连接的客户端地址:" + ctx.channel().remoteAddress());
        ctx.writeAndFlush("客户端"+ InetAddress.getLocalHost().getHostName() + "成功与服务端建立连接！ \n");
        super.channelActive(ctx);
    }
}
