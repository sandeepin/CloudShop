package com.whut.config;

import com.whut.domain.User;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 拦截配置
 *
 * @author Sandeepin
 */
//请求前处理
public class WebInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean flag = true;
        User user = (User) request.getSession().getAttribute("user");
        if (null == user) {
            System.out.println("未登录");
            response.sendRedirect("toLogin");
            flag = false;
        } else {
            System.out.println("登录的账号:" + user.getUserName() + " 密码:" + user.getPassWord());
            flag = true;
        }
        return flag;
    }

    @Override//请求后处理
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
