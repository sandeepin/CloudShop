package com.whut.vo;

/**
 * Created by qing on 2017/3/22.
 * 给前台返回的操作结果
 */
public class ResponseMsg {
    private boolean success = false;

    private String msg = "";

    private String cost = "0.000s";

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

}
