package com.whut.security.core.validate.sms;

/**
 * Created by WSJ on 2017/12/1.
 */
public class DefaultSmsCodeSender implements SmsCodeSender {
    @Override
    public void send(String mobile, String code) {
        System.out.println("向手机"+mobile+"发送短信验证码"+code);
    }
}
