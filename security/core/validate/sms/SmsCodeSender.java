package com.whut.security.core.validate.sms;

/**
 * Created by WSJ on 2017/12/1.
 */
public interface SmsCodeSender {

    void send(String mobile, String code);

}
