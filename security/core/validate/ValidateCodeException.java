package com.whut.security.core.validate;


import org.springframework.security.core.AuthenticationException;

/**
 * Created by WSJ on 2017/11/23.
 */
public class ValidateCodeException extends AuthenticationException {

    public ValidateCodeException(String msg) {
        super(msg);
    }
}
