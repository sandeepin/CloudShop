package com.whut.security.core.validate;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * Created by WSJ on 2017/11/23.
 */
public interface ValidateCodeGenerator {

    ValidateCode generate(ServletWebRequest request);

}
