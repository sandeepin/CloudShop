package com.whut.security.core.properties;

/**
 * Created by WSJ on 2017/11/23.
 */
public class ImageCodeProperties extends SmsCodeProperties{

    public ImageCodeProperties(){
        setLength(4);
    }

    private int width = 67;

    private int height = 23;

    //默认的需要验证码的拦截路径
    private String url;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
