package com.whut.security.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by WSJ on 2017/11/22.
 */
//这个类会读取配置文件中以wsj.security开头的项
@ConfigurationProperties(prefix = "wsj.security")
public class SecurityProperties {

    private BrowserProperties browserProperties = new BrowserProperties();

    private ValidateCodeProperties code = new ValidateCodeProperties();

    public BrowserProperties getBrowserProperties() {
        return browserProperties;
    }

    public void setBrowserProperties(BrowserProperties browserProperties) {
        this.browserProperties = browserProperties;
    }

    public ValidateCodeProperties getCode() {
        return code;
    }

    public void setCode(ValidateCodeProperties validateCodeProperties) {
        this.code = validateCodeProperties;
    }
}
