# CloudShop
云计算平台资源销售网站

CloudShopWeb：存放销售平台Web网站，用于与用户交互。

CloudShopQtServer：存放Qt后端处理程序，用于与Openstack通信。

doc：存放协议、配置、注意事项等说明文档。