#ifndef RESIGISTUSER_H
#define RESIGISTUSER_H

#include <QObject>
#include <qthread.h>
#include <QTcpSocket>

enum RegistRes
{
    R_OK,
    R_LOGIN_EXIST,
    R_OPENSTACK_FAILD,
    R_FILE_OPEN_FAILD
};

class ResigistUser : public QThread
{
    Q_OBJECT
public:
    explicit ResigistUser(QObject *parent = nullptr);
    ~ResigistUser();
private:
    QString login;
    QString passwd;
    QTcpSocket* m_pSocket;
    QByteArray m_passage;
public:
    void revPassage(QByteArray& passage,QTcpSocket* socket);
private:
    bool parsePassage(QByteArray& passage);
    RegistRes addUser(QByteArray& result);
    bool addUserAtOpenstack(QByteArray& result);
protected:
    void run();
signals:
    void replyRegisterResult(QTcpSocket* socket,QString userLogin,RegistRes res,QByteArray result);
public slots:
};

#endif // RESIGISTUSER_H
