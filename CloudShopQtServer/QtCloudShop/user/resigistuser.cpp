#include "resigistuser.h"
#include <QByteArray>
#include <QFile>
#include <QList>
#include <QTime>
#include <QTextStream>

static QString logpath="/home/lou/Openstack/log";
static QString temppath="/home/lou/Openstack/temp";

ResigistUser::ResigistUser(QObject *parent) : QThread(parent)
{
    qRegisterMetaType<RegistRes>("RegistRes");
}

ResigistUser::~ResigistUser()
{
    qDebug()<<"delete resigistThread";
}
void ResigistUser::revPassage(QByteArray& passage,QTcpSocket* socket)
{
    m_pSocket=socket;
    m_passage=passage;
}
bool ResigistUser::parsePassage(QByteArray& passage)
{
    QList<QByteArray> list = passage.split(',');
    if(list.size()==3)
    {
        login=list.at(1);
        passwd=list.at(2);
        return true;
    }
    return false;
}
RegistRes ResigistUser::addUser(QByteArray& result)
{
    QFile fout(logpath+'/'+login+".log");
    if(fout.exists())//if user login is exist already,add default
    {
        return R_LOGIN_EXIST;
    }
    if(!fout.open(QIODevice::WriteOnly|QIODevice::Text))
    {
        return R_FILE_OPEN_FAILD;
    }
    if(!addUserAtOpenstack(result))
    {
        fout.close();
        fout.remove();
        return R_OPENSTACK_FAILD;
    }
    QByteArray buffer;
    buffer.append(login.toLatin1()).append(',');
    buffer.append(passwd.toLatin1()).append(',');
    buffer.append("0,0,0,0,end");
    fout.write(buffer);
    fout.close();
    return R_OK;
}
bool ResigistUser::addUserAtOpenstack(QByteArray& result)
{
    QString currentTime = QTime::currentTime().toString();
    QString shPath=temppath+'/'+login+currentTime+".sh";
    QString resPath=temppath+'/'+login+currentTime+".txt";
    QFile fout(shPath);
    if(!fout.open(QIODevice::WriteOnly|QIODevice::Text))
    {
        return false;
    }
    QTextStream stream(&fout);
    stream<<"#!/bin/sh\n";
    stream<<"export OS_PROJECT_DOMAIN_ID=default\n"
          <<"export OS_USER_DOMAIN_ID=default\n"
          <<"export OS_PROJECT_NAME=admin\n"
          <<"export OS_TENANT_NAME=admin\n"
          <<"export OS_USERNAME=admin\n"
          <<"export OS_PASSWORD=123\n"
          <<"export OS_AUTH_URL=http://controller:35357/v3\n"
          <<"export OS_IDENTITY_API_VERSION=3\n";

    stream<<"openstack project create --domain default "<< login<<"\n";
    stream<<"openstack user create --domain default --password "<<passwd+" "+login<<"\n";
    stream<<"openstack role add --project "<<login+" --user "+login+" user\n";

    stream<<"tenant=$(openstack project list | awk '/"<<login+"/ {print $2}')\n";
    stream<<"nova quota-update --instances 0 $tenant\n";
    stream<<"nova quota-update --cores 0 $tenant\n";
    stream<<"nova quota-update --ram 0 $tenant\n";

    stream<<"export OS_PROJECT_DOMAIN_ID=default\n"
          <<"export OS_USER_DOMAIN_ID=default\n"
          <<"export OS_PROJECT_NAME="<<login<<"\n"
          <<"export OS_TENANT_NAME="<<login<<"\n"
          <<"export OS_USERNAME="<<login<<"\n"
          <<"export OS_PASSWORD="<<passwd<<"\n"
          <<"export OS_AUTH_URL=http://controller:5000/v3\n"
          <<"export OS_IDENTITY_API_VERSION=3\n";
    stream<<"ssh-keygen -q -N ""\n";
    stream<<"nova keypair-add --pub-key ~/.ssh/id_rsa.pub "<<login<<"key\n";
    stream<<"nova secgroup-add-rule default icmp -1 -1 0.0.0.0/0\n";
    stream<<"nova secgroup-add-rule default tcp 22 22 0.0.0.0/0\n";
    fout.close();
    QString command;
    command.append("sh ").append(shPath).append(" >> ").append(resPath);
    int res = system(command.toStdString().c_str());
    fout.remove();
    QFile fin(resPath);
    if(fin.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        result= fin.readAll();
        fin.close();
        fin.remove();
    }
    if(res==0)
    {
        return true;
    }
    return false;
}
void ResigistUser::run()
{
    if(m_passage.isEmpty())
    {
        return;
    }
    if(!parsePassage(m_passage))
    {
        return;
    }
    QByteArray result;
    RegistRes res = addUser(result);
    emit(replyRegisterResult(m_pSocket,login,res,result));
}

