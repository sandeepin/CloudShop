#include "removeuser.h"
#include <QFile>
#include <QList>
#include <QByteArray>
#include <QTextStream>
#include <QTime>

static QString temppath="/home/lou/Openstack/temp";
static QString logpath="/home/lou/Openstack/log";

RemoveUser::RemoveUser(QObject *parent) : QThread(parent)
{
    qRegisterMetaType<RemoveRes>("RemoveRes");
}
RemoveUser::~RemoveUser()
{
    qDebug()<<"delete removeThread";
}

void RemoveUser::revPassage(QByteArray& passage,QTcpSocket* socket)
{
    m_pSocket=socket;
    m_passage=passage;
}
bool RemoveUser::parsePassage(QByteArray& passage)
{
    QList<QByteArray> list = passage.split(',');
    if(list.size()==3)
    {
        login=list.at(1);
        passwd=list.at(2);
        return true;
    }
    return false;
}
RemoveRes RemoveUser::rmUser(QByteArray& result)
{
    QFile fin(logpath+'/'+login+".log");
    if(!fin.exists())//if user exist
    {
        return M_LOGIN_NOT_EXIST;
    }
    if(!fin.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        return M_FILE_OPEN_FAILD;
    }
    QByteArray line = fin.readLine();
    fin.close();
    QList<QByteArray> list = line.split(',');
    if(list.size()!=7)
    {
        return M_FORMAT_ERROR;
    }
    if(list.at(1)!=passwd)
    {
        return M_PASS_ERROR;
    }
    if(!rmUserAtOpenstack(result))
    {
        return M_OPENSTACK_FAILD;
    }
    fin.remove();
    return M_OK;
}
bool RemoveUser::rmUserAtOpenstack(QByteArray& result)
{
    QString currentTime=QTime::currentTime().toString();
    QString shPath=temppath+'/'+login+currentTime+".sh";
    QString resPath=temppath+"/"+login+currentTime+".txt";
    QFile fout(shPath);
    if(!fout.open(QIODevice::WriteOnly|QIODevice::Text))
    {
        return false;
    }
    QTextStream stream(&fout);
    stream<<"#!/bin/sh\n";
    stream<<"export OS_PROJECT_DOMAIN_ID=default\n"
          <<"export OS_USER_DOMAIN_ID=default\n"
          <<"export OS_PROJECT_NAME="<<login<<"\n"
          <<"export OS_TENANT_NAME="<<login<<"\n"
          <<"export OS_USERNAME="<<login<<"\n"
          <<"export OS_PASSWORD="<<passwd<<"\n"
          <<"export OS_AUTH_URL=http://controller:5000/v3\n"
          <<"export OS_IDENTITY_API_VERSION=3\n";
    stream<<"nova list\n";
    fout.close();
    QString command;
    command.append("sh ").append(shPath).append(" >> ").append(resPath);
    int res = system(command.toStdString().c_str());
    fout.remove();
    if(res!=0)
    {
        return false;
    }
    QFile fin(resPath);
    if(!fin.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        return false;
    }
    fin.readLine();
    fin.readLine();
    fin.readLine();
    QList<QByteArray> idList;
    while(true)
    {
        QByteArray buffer=fin.readLine();
        if(buffer.contains("+--")||buffer.isEmpty())
        {
            break;
        }
        QByteArray id = buffer.mid(2,36);
        idList.push_back(id);
    }
    fin.close();
    fin.remove();

    QFile fout2(shPath);
    if(!fout2.open(QIODevice::WriteOnly|QIODevice::Text))
    {
        return false;
    }
    QTextStream stream2(&fout2);
    stream2<<"#!/bin/sh\n";
    stream2<<"export OS_PROJECT_DOMAIN_ID=default\n"
          <<"export OS_USER_DOMAIN_ID=default\n"
          <<"export OS_PROJECT_NAME=admin\n"
          <<"export OS_TENANT_NAME=admin\n"
          <<"export OS_USERNAME=admin\n"
          <<"export OS_PASSWORD=123\n"
          <<"export OS_AUTH_URL=http://controller:35357/v3\n"
          <<"export OS_IDENTITY_API_VERSION=3\n";
    for(auto &id:idList)
    {
        stream2<<"nova delete "<<id<<"\n";
    }
    stream2<<"openstack project delete --domain default "<< login<<"\n";
    stream2<<"openstack user delete --domain default "<<login<<"\n";
    fout2.close();
    res = system(command.toStdString().c_str());
    fout2.remove();
    QFile fin2(resPath);
    if(fin2.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        result= fin2.readAll();
        fin2.close();
        fin2.remove();
    }
    if(res==0)
    {
        return true;
    }
    return false;
}
void RemoveUser::run()
{
    if(m_passage.isEmpty())
    {
        return;
    }
    if(!parsePassage(m_passage))
    {
        return;
    }
    QByteArray result;
    RemoveRes res = rmUser(result);
    emit(replyRemoveResult(m_pSocket,login,res,result));
}
