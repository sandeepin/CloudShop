#ifndef REMOVEUSER_H
#define REMOVEUSER_H

#include <QObject>
#include <qthread.h>
#include <QTcpSocket>

enum RemoveRes
{
    M_OK,
    M_LOGIN_NOT_EXIST,
    M_FILE_OPEN_FAILD,
    M_OPENSTACK_FAILD,
    M_PASS_ERROR,
    M_FORMAT_ERROR
};
class RemoveUser : public QThread
{
    Q_OBJECT
public:
    explicit RemoveUser(QObject *parent = nullptr);
    ~RemoveUser();
private:
    QString login;
    QString passwd;
    QTcpSocket* m_pSocket;
    QByteArray m_passage;
public:
    void revPassage(QByteArray& passage,QTcpSocket* socket);
private:
    bool parsePassage(QByteArray& passage);
    RemoveRes rmUser(QByteArray& result);
    bool rmUserAtOpenstack(QByteArray& result);
protected:
    void run();
signals:
      void replyRemoveResult(QTcpSocket* socket,QString userLogin,RemoveRes res,QByteArray result);
};

#endif // REMOVEUSER_H
