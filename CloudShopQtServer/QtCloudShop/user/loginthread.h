#ifndef LOGINTHREAD_H
#define LOGINTHREAD_H

#include <QObject>
#include <QThread>
#include <QTcpSocket>

enum LoginRes
{
    L_OK,
    L_FILE_OPEN_FAILED,
    L_IDENTITY_ERROR,
    L_OPENSTACK_FAILD
};
class LoginThread : public QThread
{
    Q_OBJECT
public:
    explicit LoginThread(QObject *parent = nullptr);
    ~LoginThread();
private:
    QString login;
    QString passwd;
    QByteArray m_passage;
    QTcpSocket* m_pSocket;

protected:
    bool parsePassage(QByteArray& passage);
    LoginRes synData(QByteArray& result);
    void run();
public:
    void revPassage(QByteArray& passage,QTcpSocket* socket);
signals:
    void replyLoginResult(QTcpSocket* socket,QString user,LoginRes res,QByteArray result);
public slots:
};

#endif // LOGINTHREAD_H
