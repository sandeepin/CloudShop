#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <queue>
#include "util/usersquota.h"
#include "user/resigistuser.h"
#include "vohst/create.h"
#include "vohst/terminatethread.h"
#include "user/removeuser.h"
#include "user/loginthread.h"

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(int port,QObject *parent = nullptr);
    ~Server();


private:
    std::queue<char> *m_pQueue; //all recieved
    QList<QTcpSocket*> m_clients;//clients list
    QTcpServer* m_pServer;//server SOCKET

private:
    void getPassage(QByteArray& passage);
    int parsePassage(QByteArray& passage);

public slots:
    void addClient();
    void removeClient();
    void readBuffer();

    void revCreateResult(QTcpSocket* socket,VM vm,bool res,QByteArray result);
    void revTerminateResult(QTcpSocket* socket,QString user,QString vmId,QByteArray result);
    void revRegistResult(QTcpSocket* socket,QString userLogin,RegistRes res,QByteArray result);
    void revRemoveResult(QTcpSocket* socket,QString userLogin,RemoveRes res,QByteArray result);
    void revLoginResult(QTcpSocket* socket,QString userLogin,LoginRes res,QByteArray result);
signals:

};

#endif // SERVER_H
