#ifndef USERQUOTA_H
#define USERQUOTA_H
#include <QString>



struct Quota
{
    Quota(){}
    Quota(QString usr,QString pass,QString vm,QString vc,QString rm,QString dk)
    {
        user=usr;passwd=pass;vmNum=vm;vcups=vc;ram=rm;disk=dk;
    }
    QString user;
    QString passwd;
    QString vmNum;
    QString vcups;
    QString ram;
    QString disk;
};

class UsersQuota
{
public:
    UsersQuota();
    ~UsersQuota();

public:
    bool checkIdentity(const QString& user,const QString& password);
    bool addAndGetQuota(const QString& user,int cpus,int ram,int disk,Quota& curQuota);
    bool cutAndGetQuota(const QString& user,int cpus,int ram,int disk,Quota& curQuota);
    bool saveQuota(Quota& quota);
private:
    bool changeQuota(const QString& user,int cpus,int ram,int disk,int vm,Quota &quota);
};

#endif // USERQUOTA_H
