#include <QCoreApplication>
#include "network/server.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    new Server(6081);
    return a.exec();
}
