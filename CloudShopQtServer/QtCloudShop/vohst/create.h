#ifndef CREATE_H
#define CREATE_H

#include <QObject>
#include <qthread.h>
#include <QTcpSocket>
struct VM
{
    QString userName;
    QString password;
    QString vmName;
    QString vmId;
    QString vmImage;
    QString vcpus;
    QString ram; //GB
    QString disk;//MB
};
class Create : public QThread
{
    Q_OBJECT
public:
    explicit Create(QObject *parent = nullptr);
    ~Create();
private:
    QByteArray m_passage;
    QTcpSocket* m_pSocket;
protected:
    bool parsePassage(QByteArray& passage,VM& vm);
    bool createVm(VM& vm,QByteArray& result);
    void run();
public:
    void revPassage(QByteArray& passage,QTcpSocket* socket);
signals:
    void replyCreateResult(QTcpSocket* socket,VM vm,bool res,QByteArray result);

};

#endif // VIRMACHINE_H
