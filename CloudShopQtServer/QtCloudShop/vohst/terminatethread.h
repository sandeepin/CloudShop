#ifndef TERMINATETHREAD_H
#define TERMINATETHREAD_H

#include <QObject>
#include <qthread.h>
#include <QTcpSocket>

class TerminateThread : public QThread
{
    Q_OBJECT
public:
    explicit TerminateThread(QObject *parent = nullptr);
    ~TerminateThread();
private:

    QString login;
    QString password;
    QString vmId;
    QString cpu,ram,disk;
    QTcpSocket* m_pSocket;
    QByteArray m_passage;
public:
    void revPassage(QByteArray& passage,QTcpSocket* socket);
private:
    bool parsePassage(QByteArray& passage);
    bool terminateVm(QByteArray& result);

protected:
    void run();
signals:
    void replyTerminateResult(QTcpSocket* socket,QString user,QString id,QByteArray result);

public slots:
};

#endif // TERMINATETHREAD_H
