#include "terminatethread.h"
#include <QByteArray>
#include <QFile>
#include <QList>
#include <QTextStream>
#include <QTime>
#include "util/usersquota.h"

static QString temppath="/home/lou/Openstack/temp";

TerminateThread::TerminateThread(QObject *parent) : QThread(parent)
{

}
TerminateThread::~TerminateThread()
{
      qDebug()<<"delete terminateThread";
}

void TerminateThread::revPassage(QByteArray& passage,QTcpSocket* socket)
{
    m_passage=passage;
    m_pSocket=socket;
}
bool TerminateThread::parsePassage(QByteArray& passage)
{
    QList<QByteArray> list = passage.split(',');
    if(list.size()==7)
    {
        login=list.at(1);
        password=list.at(2);
        vmId=list.at(3);
        cpu=list.at(4);
        ram=list.at(5);
        disk=list.at(6);
        return true;
    }
    return false;
}
bool TerminateThread::terminateVm(QByteArray& result)
{
    UsersQuota usersQuota;
    if(!usersQuota.checkIdentity(login,password))
    {
        return false;
    }
    QString currentTime = QTime::currentTime().toString();
    QString shPath=temppath+'/'+login+currentTime+".sh";
    QString resPath=temppath+'/'+login+currentTime+".txt";
    QFile fout(shPath);
    if(!fout.open(QIODevice::WriteOnly|QIODevice::Text))
    {
        return false;
    }
    QTextStream stream(&fout);
    Quota curQuota;
    int cutCpu=cpu.toInt();
    int cutRam=ram.toInt();
    int cutDisk=disk.toInt();
    if(!usersQuota.cutAndGetQuota(login,cutCpu,cutRam,cutDisk,curQuota))//the function doesn't change file log
    {
        return false;
    }
    stream<<"#!/bin/sh\n";
    stream<<"export OS_PROJECT_DOMAIN_ID=default\n"
          <<"export OS_USER_DOMAIN_ID=default\n"
          <<"export OS_PROJECT_NAME=admin\n"
          <<"export OS_TENANT_NAME=admin\n"
          <<"export OS_USERNAME=admin\n"
          <<"export OS_PASSWORD=123\n"
          <<"export OS_AUTH_URL=http://controller:35357/v3\n"
          <<"export OS_IDENTITY_API_VERSION=3\n";

//    stream<<"export OS_PROJECT_DOMAIN_ID=default\n"
//          <<"export OS_USER_DOMAIN_ID=default\n"
//          <<"export OS_PROJECT_NAME="<<login<<"\n"
//          <<"export OS_TENANT_NAME="<<login<<"\n"
//          <<"export OS_USERNAME="<<login<<"\n"
//          <<"export OS_PASSWORD="<<password<<"\n"
//          <<"export OS_AUTH_URL=http://controller:5000/v3\n"
//          <<"export OS_IDENTITY_API_VERSION=3\n";

    stream<<"nova delete "<<vmId<<"\n";
    stream<<"tenant=$(openstack project list | awk '/"<<login+"/ {print $2}')\n";
    stream<<"nova quota-update --instances "<<curQuota.vmNum<<" $tenant\n";
    stream<<"nova quota-update --cores "+curQuota.vcups+" $tenant\n";
    stream<<"nova quota-update --ram "+curQuota.ram+" $tenant\n";
    fout.close();
    QString command;
    command.append("sh ").append(shPath).append(" >> ").append(resPath);
    int res = system(command.toStdString().c_str());
    fout.remove();
    QFile fin(resPath);
    if(fin.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        result= fin.readAll();
        fin.close();
        fin.remove();
    }
    if(res!=0)
    {
        return false;
    }
    usersQuota.saveQuota(curQuota);
    return true;
}
void TerminateThread::run()
{
   if(m_passage.isEmpty())
   {
       return;
   }
   if(!parsePassage(m_passage))
   {
       return;
   }
   QByteArray result;
   terminateVm(result);
   emit(replyTerminateResult(m_pSocket,login,vmId,result));
}
